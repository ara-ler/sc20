// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2021
 * Author(s): Giulio Benetti <giulio.benetti@benettiengineering.com>
 */

#include <linux/delay.h>
#include <linux/gpio/consumer.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/input/mt.h>
#include <linux/input/touchscreen.h>
#include <linux/irq.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/regulator/consumer.h>
#include <linux/regmap.h>
#include <linux/module.h>
#include <linux/uaccess.h>

#include <asm/unaligned.h>

#define HY46XX_CHKSUM_CODE		0x1
#define HY46XX_FINGER_NUM		0x2
#define HY46XX_CHKSUM_LEN		0x7
#define HY46XX_THRESHOLD		0x80
#define HY46XX_GLOVE_EN			0x84
#define HY46XX_REPORT_SPEED		0x88
#define HY46XX_PWR_NOISE_EN		0x89
#define HY46XX_FILTER_DATA		0x8A
#define HY46XX_GAIN				0x92
#define HY46XX_EDGE_OFFSET		0x93
#define HY46XX_RX_NR_USED		0x94
#define HY46XX_TX_NR_USED		0x95
#define HY46XX_PWR_MODE			0xA5
#define HY46XX_FW_VERSION		0xA6
#define HY46XX_LIB_VERSION		0xA7
#define HY46XX_TP_INFO			0xA8
#define HY46XX_TP_CHIP_ID		0xA9
#define HY46XX_BOOT_VER			0xB0

#define HY46XX_TPLEN			0x6
#define HY46XX_REPORT_PKT_LEN		0x44

/* by_SM */
// #define HY46XX_MAX_SUPPORTED_POINTS	11
#define HY46XX_MAX_SUPPORTED_POINTS	2

#define TOUCH_EVENT_DOWN		0x00
#define TOUCH_EVENT_UP			0x01
#define TOUCH_EVENT_CONTACT		0x02
#define TOUCH_EVENT_RESERVED	0x03

#define DEVICE_NAME "bbbi2c"
#define DEVICE_CLASS_NAME "bbbi2c_class"
#define DEVICE_PROCESS "bbbi2c%d"

#define driver_info(format,arg...)	do { printk( KERN_INFO format, ## arg ); } while(0)
#define driver_err(format,arg...)	do { printk( KERN_ERR  format, ## arg ); } while(0)

#define ADDR_IODEV      0x27  
#define INPUT_PORT_0    0x00  
#define INPUT_PORT_1    0x01  
#define OUTPUT_PORT_0   0x02  
#define OUTPUT_PORT_1   0x03  
#define CONFIG_PORT_0   0x06  
#define CONFIG_PORT_1   0x07

#define IN_ISR_TRUE		in_isr = true
#define IN_ISR_FALSE	usleep_range(1280, 2000); in_isr = false

#define DRIVER_VERSION	23044

extern int devm_device_add_group(struct device *dev, const struct attribute_group *grp);

/* by_SM */
static volatile bool in_isr;
static struct	i2c_adapter *adapter;
static bool		bad_state = false;

static int 		bbbi2c_open (struct inode*, struct file*);
static int 		bbbi2c_release (struct inode*, struct file*);
static long 	bbbi2c_ioctl (struct file *, unsigned int, unsigned long );
static ssize_t 	bbbi2c_read (struct file *, char __user*, size_t, loff_t*);
static ssize_t 	bbbi2c_write (struct file *, const char __user *, size_t, loff_t *);

/* bbbi2c device structure */
struct bbbi2c_device {
	struct cdev cdev;
	struct device *dev_ptr;
	u8 is_open;
};

static struct 			bbbi2c_device *bbbi2c_dev_ptr = NULL;
static dev_t 			bbbi2c_dev_no;
static struct 			class *bbbi2cclass_ptr = NULL;
static int				debug = 0;				/* debug flag to enable/disable extra messages via printk() */

static struct 			file_operations fops =
{
	.open = bbbi2c_open,
	.release = bbbi2c_release,
	.unlocked_ioctl = bbbi2c_ioctl,
	.read = bbbi2c_read,
	.write = bbbi2c_write
};

struct hycon_hy46xx_data {
	struct i2c_client *client;
	struct input_dev *input;
	struct touchscreen_properties prop;
	struct regulator *vcc;

	struct gpio_desc *reset_gpio;

	struct mutex mutex;
	struct regmap *regmap;

	int threshold;
	bool glove_enable;
	int report_speed;
	bool noise_filter_enable;
	int filter_data;
	int gain;
	int edge_offset;
	int rx_number_used;
	int tx_number_used;
	int power_mode;
	int fw_version;
	int lib_version;
	int tp_information;
	int tp_chip_id;
	int bootloader_version;
};

static const struct regmap_config hycon_hy46xx_i2c_regmap_config = {
	.reg_bits = 8,
	.val_bits = 8,
};

void i2c_reg_write(struct i2c_adapter *i2c, u8 device_address, u8 reg_index, u8 new_reg_value)
{
    struct i2c_msg msg;
    u8 buf[2] = {reg_index, new_reg_value};

    msg.addr = (__u16)device_address;
    msg.flags = 0;
    msg.len = 2;
    msg.buf = buf;

    i2c_transfer(i2c, &msg, 1);
}

/* by_SM */
static u8 i2c_reg_read(struct i2c_adapter *i2c, u8 device_address, u8 reg_index)
{
    struct i2c_msg msg[2];
    u8 reg_value;

    msg[0].addr = (__u16)device_address;
    msg[0].flags = 0;
    msg[0].len = 1;
    msg[0].buf = &reg_index;
    msg[1].addr = (__u16)device_address;
    msg[1].flags = I2C_M_RD;
    msg[1].len = 1;
    msg[1].buf = &reg_value;

    i2c_transfer(i2c, msg, 2);

    return reg_value;
}

static bool hycon_hy46xx_check_checksum(struct hycon_hy46xx_data *tsdata, u8 *buf)
{
	u8 chksum = 0;
	int i;

	for (i = 2; i < buf[HY46XX_CHKSUM_LEN]; i++)
		chksum += buf[i];

	if (chksum == buf[HY46XX_CHKSUM_CODE])
		return true;

	dev_err_ratelimited(&tsdata->client->dev, "checksum error: 0x%02x expected, got 0x%02x\n", chksum, buf[HY46XX_CHKSUM_CODE]);

	return false;
}

static irqreturn_t hycon_hy46xx_isr(int irq, void *dev_id)
{
	struct hycon_hy46xx_data *tsdata = dev_id;
	struct device *dev = &tsdata->client->dev;
	u8 rdbuf[HY46XX_REPORT_PKT_LEN];
	int i, x, y, id;
	int error;

	if (in_isr)
		goto out;

	IN_ISR_TRUE;
	memset(rdbuf, 0, sizeof(rdbuf));

	// by_SM
	//dev_err(&tsdata->client->dev, "==== Entered ISR ====\n");

	error = regmap_bulk_read(tsdata->regmap, 0, rdbuf, sizeof(rdbuf));
	if (error) {
		dev_err_ratelimited(dev, "Unable to fetch data, error: %d\n", error);
		bad_state = true;
		//devm_free_irq(dev, tsdata->client->irq, dev_id);
		goto out;
	}

	if (!hycon_hy46xx_check_checksum(tsdata, rdbuf))
		goto out;

	for (i = 0; i < HY46XX_MAX_SUPPORTED_POINTS; i++) {
		u8 *buf = &rdbuf[3 + (HY46XX_TPLEN * i)];
		int type = buf[0] >> 6;

		if (type == TOUCH_EVENT_RESERVED)
			continue;

		x = get_unaligned_be16(buf) & 0x0fff;
		y = get_unaligned_be16(buf + 2) & 0x0fff;

		id = buf[2] >> 4;

		input_mt_slot(tsdata->input, id);
		if (input_mt_report_slot_state(tsdata->input, MT_TOOL_FINGER, type != TOUCH_EVENT_UP))
			touchscreen_report_pos(tsdata->input, &tsdata->prop, x, y, true);
	}

	input_mt_report_pointer_emulation(tsdata->input, false);
	input_sync(tsdata->input);

out:
	IN_ISR_FALSE;
	return IRQ_HANDLED;
}

struct hycon_hy46xx_attribute {
	struct device_attribute dattr;
	size_t field_offset;
	u8 address;
	u8 limit_low;
	u8 limit_high;
};

#define HYCON_ATTR_U8(_field, _mode, _address, _limit_low, _limit_high)	\
	struct hycon_hy46xx_attribute hycon_hy46xx_attr_##_field = {		\
		.dattr = __ATTR(_field, _mode,				\
				hycon_hy46xx_setting_show,			\
				hycon_hy46xx_setting_store),			\
		.field_offset = offsetof(struct hycon_hy46xx_data, _field),	\
		.address = _address,					\
		.limit_low = _limit_low,				\
		.limit_high = _limit_high,				\
	}

#define HYCON_ATTR_BOOL(_field, _mode, _address)			\
	struct hycon_hy46xx_attribute hycon_hy46xx_attr_##_field = {		\
		.dattr = __ATTR(_field, _mode,				\
				hycon_hy46xx_setting_show,			\
				hycon_hy46xx_setting_store),			\
		.field_offset = offsetof(struct hycon_hy46xx_data, _field),	\
		.address = _address,					\
		.limit_low = false,					\
		.limit_high = true,					\
	}

static ssize_t hycon_hy46xx_setting_show(struct device *dev,
				   struct device_attribute *dattr, char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct hycon_hy46xx_data *tsdata = i2c_get_clientdata(client);
	struct hycon_hy46xx_attribute *attr =
			container_of(dattr, struct hycon_hy46xx_attribute, dattr);
	u8 *field = (u8 *)tsdata + attr->field_offset;
	size_t count = 0;
	int error = 0;
	int val;

	mutex_lock(&tsdata->mutex);

	error = regmap_read(tsdata->regmap, attr->address, &val);
	if (error < 0) {
		dev_err(&tsdata->client->dev,
			"Failed to fetch attribute %s, error %d\n",
			dattr->attr.name, error);
		goto out;
	}

	if (val != *field) {
		dev_warn(&tsdata->client->dev,
			 "%s: read (%d) and stored value (%d) differ\n",
			 dattr->attr.name, val, *field);
		*field = val;
	}

	count = scnprintf(buf, PAGE_SIZE, "%d\n", val);

out:
	mutex_unlock(&tsdata->mutex);
	return error ?: count;
}

static ssize_t hycon_hy46xx_setting_store(struct device *dev,
					struct device_attribute *dattr,
					const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct hycon_hy46xx_data *tsdata = i2c_get_clientdata(client);
	struct hycon_hy46xx_attribute *attr =
			container_of(dattr, struct hycon_hy46xx_attribute, dattr);
	u8 *field = (u8 *)tsdata + attr->field_offset;
	unsigned int val;
	int error;

	mutex_lock(&tsdata->mutex);

	error = kstrtouint(buf, 0, &val);
	if (error)
		goto out;

	if (val < attr->limit_low || val > attr->limit_high) {
		error = -ERANGE;
		goto out;
	}

	error = regmap_write(tsdata->regmap, attr->address, val);
	if (error < 0) {
		dev_err(&tsdata->client->dev,
			"Failed to update attribute %s, error: %d\n",
			dattr->attr.name, error);
		goto out;
	}
	*field = val;

out:
	mutex_unlock(&tsdata->mutex);
	return error ?: count;
}

static HYCON_ATTR_U8(threshold, 0644, HY46XX_THRESHOLD, 0, 255);
static HYCON_ATTR_BOOL(glove_enable, 0644, HY46XX_GLOVE_EN);
static HYCON_ATTR_U8(report_speed, 0644, HY46XX_REPORT_SPEED, 0, 255);
static HYCON_ATTR_BOOL(noise_filter_enable, 0644, HY46XX_PWR_NOISE_EN);
static HYCON_ATTR_U8(filter_data, 0644, HY46XX_FILTER_DATA, 0, 5);
static HYCON_ATTR_U8(gain, 0644, HY46XX_GAIN, 0, 5);
static HYCON_ATTR_U8(edge_offset, 0644, HY46XX_EDGE_OFFSET, 0, 5);
static HYCON_ATTR_U8(fw_version, 0444, HY46XX_FW_VERSION, 0, 255);
static HYCON_ATTR_U8(lib_version, 0444, HY46XX_LIB_VERSION, 0, 255);
static HYCON_ATTR_U8(tp_information, 0444, HY46XX_TP_INFO, 0, 255);
static HYCON_ATTR_U8(tp_chip_id, 0444, HY46XX_TP_CHIP_ID, 0, 255);
static HYCON_ATTR_U8(bootloader_version, 0444, HY46XX_BOOT_VER, 0, 255);

static struct attribute *hycon_hy46xx_attrs[] = {
	&hycon_hy46xx_attr_threshold.dattr.attr,
	&hycon_hy46xx_attr_glove_enable.dattr.attr,
	&hycon_hy46xx_attr_report_speed.dattr.attr,
	&hycon_hy46xx_attr_noise_filter_enable.dattr.attr,
	&hycon_hy46xx_attr_filter_data.dattr.attr,
	&hycon_hy46xx_attr_gain.dattr.attr,
	&hycon_hy46xx_attr_edge_offset.dattr.attr,
	&hycon_hy46xx_attr_fw_version.dattr.attr,
	&hycon_hy46xx_attr_lib_version.dattr.attr,
	&hycon_hy46xx_attr_tp_information.dattr.attr,
	&hycon_hy46xx_attr_tp_chip_id.dattr.attr,
	&hycon_hy46xx_attr_bootloader_version.dattr.attr,
	NULL
};

static const struct attribute_group hycon_hy46xx_attr_group = {
	.attrs = hycon_hy46xx_attrs,
};

static void hycon_hy46xx_get_defaults(struct device *dev, struct hycon_hy46xx_data *tsdata)
{
	bool val_bool;
	int error;
	u32 val;

	error = device_property_read_u32(dev, "hycon,threshold", &val);
	if (!error) {
		error = regmap_write(tsdata->regmap, HY46XX_THRESHOLD, val);
		if (error < 0)
			goto out;

		tsdata->threshold = val;
	}

	val_bool = device_property_read_bool(dev, "hycon,glove-enable");
	error = regmap_write(tsdata->regmap, HY46XX_GLOVE_EN, val_bool);
	if (error < 0)
		goto out;
	tsdata->glove_enable = val_bool;

	error = device_property_read_u32(dev, "hycon,report-speed-hz", &val);
	if (!error) {
		error = regmap_write(tsdata->regmap, HY46XX_REPORT_SPEED, val);
		if (error < 0)
			goto out;

		tsdata->report_speed = val;
	}

	val_bool = device_property_read_bool(dev, "hycon,noise-filter-enable");
	error = regmap_write(tsdata->regmap, HY46XX_PWR_NOISE_EN, val_bool);
	if (error < 0)
		goto out;
	tsdata->noise_filter_enable = val_bool;

	error = device_property_read_u32(dev, "hycon,filter-data", &val);
	if (!error) {
		error = regmap_write(tsdata->regmap, HY46XX_FILTER_DATA, val);
		if (error < 0)
			goto out;

		tsdata->filter_data = val;
	}

	error = device_property_read_u32(dev, "hycon,gain", &val);
	if (!error) {
		error = regmap_write(tsdata->regmap, HY46XX_GAIN, val);
		if (error < 0)
			goto out;

		tsdata->gain = val;
	}

	error = device_property_read_u32(dev, "hycon,edge-offset", &val);
	if (!error) {
		error = regmap_write(tsdata->regmap, HY46XX_EDGE_OFFSET, val);
		if (error < 0)
			goto out;

		tsdata->edge_offset = val;
	}

	return;
out:
	dev_err(&tsdata->client->dev, "Failed to set default settings");
}

static void hycon_hy46xx_get_parameters(struct hycon_hy46xx_data *tsdata)
{
	int error;
	u32 val;

	error = regmap_read(tsdata->regmap, HY46XX_THRESHOLD, &val);
	if (error < 0)
		goto out;
	tsdata->threshold = val;

	error = regmap_read(tsdata->regmap, HY46XX_GLOVE_EN, &val);
	if (error < 0)
		goto out;
	tsdata->glove_enable = val;

	error = regmap_read(tsdata->regmap, HY46XX_REPORT_SPEED, &val);
	if (error < 0)
		goto out;
	tsdata->report_speed = val;

	error = regmap_read(tsdata->regmap, HY46XX_PWR_NOISE_EN, &val);
	if (error < 0)
		goto out;
	tsdata->noise_filter_enable = val;

	error = regmap_read(tsdata->regmap, HY46XX_FILTER_DATA, &val);
	if (error < 0)
		goto out;
	tsdata->filter_data = val;

	error = regmap_read(tsdata->regmap, HY46XX_GAIN, &val);
	if (error < 0)
		goto out;
	tsdata->gain = val;

	error = regmap_read(tsdata->regmap, HY46XX_EDGE_OFFSET, &val);
	if (error < 0)
		goto out;
	tsdata->edge_offset = val;

	error = regmap_read(tsdata->regmap, HY46XX_RX_NR_USED, &val);
	if (error < 0)
		goto out;
	tsdata->rx_number_used = val;

	error = regmap_read(tsdata->regmap, HY46XX_TX_NR_USED, &val);
	if (error < 0)
		goto out;
	tsdata->tx_number_used = val;

	error = regmap_read(tsdata->regmap, HY46XX_PWR_MODE, &val);
	if (error < 0)
		goto out;
	tsdata->power_mode = val;

	error = regmap_read(tsdata->regmap, HY46XX_FW_VERSION, &val);
	if (error < 0)
		goto out;
	tsdata->fw_version = val;

	error = regmap_read(tsdata->regmap, HY46XX_LIB_VERSION, &val);
	if (error < 0)
		goto out;
	tsdata->lib_version = val;

	error = regmap_read(tsdata->regmap, HY46XX_TP_INFO, &val);
	if (error < 0)
		goto out;
	tsdata->tp_information = val;

	error = regmap_read(tsdata->regmap, HY46XX_TP_CHIP_ID, &val);
	if (error < 0)
		goto out;
	tsdata->tp_chip_id = val;

	error = regmap_read(tsdata->regmap, HY46XX_BOOT_VER, &val);
	if (error < 0)
		goto out;
	tsdata->bootloader_version = val;

	return;
out:
	dev_err(&tsdata->client->dev, "Failed to read default settings");
}

static void hycon_hy46xx_disable_regulator(void *arg)
{
	struct hycon_hy46xx_data *data = arg;

	regulator_disable(data->vcc);
}

static int hycon_hy46xx_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct hycon_hy46xx_data *tsdata;
	struct input_dev *input;
	int error;

	bad_state = false;
	dev_info(&client->dev, "probing for HYCON HY46XX I2C\n");

	tsdata = devm_kzalloc(&client->dev, sizeof(*tsdata), GFP_KERNEL);
	if (!tsdata)
		return -ENOMEM;

	tsdata->vcc = devm_regulator_get(&client->dev, "vcc");
	if (IS_ERR(tsdata->vcc)) {
		error = PTR_ERR(tsdata->vcc);
		if (error != -EPROBE_DEFER)
			dev_err(&client->dev,
				"failed to request regulator: %d\n", error);
		// by_SM return error;
	}

	error = regulator_enable(tsdata->vcc);
	if (error < 0) {
		dev_err(&client->dev, "failed to enable vcc: %d\n", error);
		// by_SM return error;
	}

	error = devm_add_action_or_reset(&client->dev,
					 hycon_hy46xx_disable_regulator,
					 tsdata);
	//if (error)
		// by_SM return error;

	tsdata->reset_gpio = devm_gpiod_get_optional(&client->dev,
						     "reset", GPIOD_OUT_LOW);
	if (IS_ERR(tsdata->reset_gpio)) {
		error = PTR_ERR(tsdata->reset_gpio);
		dev_err(&client->dev,
			"Failed to request GPIO reset pin, error %d\n", error);
		// by_SM return error;
	}

	if (tsdata->reset_gpio) {
		usleep_range(5000, 6000);
		gpiod_set_value_cansleep(tsdata->reset_gpio, 1);
		usleep_range(5000, 6000);
		gpiod_set_value_cansleep(tsdata->reset_gpio, 0);
		msleep(1000);
	}

	input = devm_input_allocate_device(&client->dev);
	if (!input) {
		dev_err(&client->dev, "failed to allocate input device.\n");
		return -ENOMEM;
	}

	mutex_init(&tsdata->mutex);
	tsdata->client = client;
	tsdata->input = input;

	tsdata->regmap = devm_regmap_init_i2c(client,
					      &hycon_hy46xx_i2c_regmap_config);
	if (IS_ERR(tsdata->regmap)) {
		dev_err(&client->dev, "regmap allocation failed\n");
		return PTR_ERR(tsdata->regmap);
	}

	hycon_hy46xx_get_defaults(&client->dev, tsdata);
	hycon_hy46xx_get_parameters(tsdata);

	input->name = "Hycon Capacitive Touch";
	input->id.bustype = BUS_I2C;
	input->dev.parent = &client->dev;

	input_set_abs_params(input, ABS_MT_POSITION_X, 0, -1, 0, 0);
	input_set_abs_params(input, ABS_MT_POSITION_Y, 0, -1, 0, 0);

	touchscreen_parse_properties(input, true, &tsdata->prop);

	error = input_mt_init_slots(input, HY46XX_MAX_SUPPORTED_POINTS, INPUT_MT_DIRECT);
	if (error) {
		dev_err(&client->dev, "Unable to init MT slots.\n");
		// by_SM return error;
	}

	i2c_set_clientdata(client, tsdata);

	error = devm_request_threaded_irq(&client->dev, client->irq,
					  NULL, hycon_hy46xx_isr, IRQF_ONESHOT,
					  client->name, tsdata);
	if (error) {
		dev_err(&client->dev, "Unable to request touchscreen IRQ.\n");
		// by_SM return error;
	}

	error = devm_device_add_group(&client->dev, &hycon_hy46xx_attr_group);
	if (error)
		return error;

	error = input_register_device(input);
	if (error)
		return error;

	// by_SM
	//dev_info(&client->dev,
	dev_err(&client->dev,
		"HYCON HY46XX initialized: IRQ %d, Reset pin %d.\n",
		client->irq,
		tsdata->reset_gpio ? desc_to_gpio(tsdata->reset_gpio) : -1);

	if (NULL == adapter) {
		adapter = to_i2c_adapter(client->dev.parent);
		if (adapter != NULL)
			dev_err(&client->dev, "Current I2C master: %s\n", adapter->name);
		else {
			dev_err(&client->dev, "Cannot determine I2C master!\n");
			return 0;
		}

		/* configure IOs */
		i2c_reg_write(adapter, ADDR_IODEV, CONFIG_PORT_0, 0xFF);
		i2c_reg_write(adapter, ADDR_IODEV, CONFIG_PORT_1, 0x00);
		i2c_reg_write(adapter, ADDR_IODEV, OUTPUT_PORT_1, 0x00);
	}
	return 0;
}

static int 
bbbi2c_open(struct inode *inode,struct file *file)
{
	bbbi2c_dev_ptr->is_open = 1;
	//driver_info ("%s: Device opened\n", DEVICE_NAME);
	return 0;     
}

static int
bbbi2c_release(struct inode *inode, struct file *file)
{
	//driver_info ("%s: Device closed\n", DEVICE_NAME);
	bbbi2c_dev_ptr->is_open = 0;
	return 0;
}

static long
bbbi2c_ioctl (struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
	driver_err ("%s: Device IOCTL\n", DEVICE_NAME);
	bbbi2c_dev_ptr->is_open = 0;
	return 0;
}

static ssize_t 
bbbi2c_read (struct file *filp, char __user *buffer, size_t length, loff_t *offset)
{
	u8 reg[2];

	if (NULL == adapter) return -ENODEV;

	while (in_isr) {
		driver_err("%s: Waiting for I2C release...\n", DEVICE_NAME);
		usleep_range(20000, 50000);
	}
	IN_ISR_TRUE;
	reg[0] = i2c_reg_read(adapter, ADDR_IODEV, INPUT_PORT_0);
	/* to read the values from the second port i.e. outputs */
	if (length > 1)
		reg[1] = i2c_reg_read(adapter, ADDR_IODEV, INPUT_PORT_1);

	IN_ISR_FALSE;

	if (copy_to_user (buffer, reg, length) !=0 ) {
		driver_err("%s: Read operation: cannot send data to userspace!\n", DEVICE_NAME);
		return -EIO;
	}
	/* just for debug
	driver_err ("%s: Read DIN: %d\n", DEVICE_NAME, reg_din);
	*/
	return 0;
}

static ssize_t 
bbbi2c_write (struct file *filp, const char __user *buffer, size_t length, loff_t *offset)
{
	u8 reg_dout;

	if (NULL == adapter) return -ENODEV;

	if (copy_from_user (&reg_dout, buffer, sizeof(reg_dout)) != 0) {
		driver_err ("%s: Write operation: cannot copy data from userspace!\n", DEVICE_NAME);
		return -EINVAL;
	}

	while (in_isr) {
		driver_err("%s: Waiting for I2C release...\n", DEVICE_NAME);
		usleep_range(20000, 50000);
	}
	/* just for debug
	driver_err("%s: Sending %02X\n", DEVICE_NAME, reg_dout);
	*/

	IN_ISR_TRUE;
	i2c_reg_write(adapter, ADDR_IODEV, OUTPUT_PORT_1, reg_dout);
	IN_ISR_FALSE;

	/* FIXME - must return current offset */
	return 1;
}

static int __init bbbi2c_init(void)
{
	bbbi2c_dev_ptr = kmalloc(sizeof(struct bbbi2c_device),GFP_KERNEL);
	if (bbbi2c_dev_ptr == NULL) {
		driver_err ("%s: Failed to allocate memory for bbbi2c_dev_ptr\n", DEVICE_NAME);
		goto failed_alloc;
	}
	memset(bbbi2c_dev_ptr, 0,sizeof(struct bbbi2c_device));
	if (alloc_chrdev_region(&bbbi2c_dev_no, 0, 1, DEVICE_NAME) < 0) {
		driver_err ("%s: Cannot register\n", DEVICE_NAME);
		goto failed_register;
	}
	bbbi2cclass_ptr=class_create(THIS_MODULE, DEVICE_CLASS_NAME);
	if (IS_ERR(bbbi2cclass_ptr)) {
		driver_err ("%s: Cannot create class\n", DEVICE_NAME);
		goto failed_class_create;
	}
	cdev_init(&(bbbi2c_dev_ptr->cdev),&fops);
	bbbi2c_dev_ptr->cdev.owner=THIS_MODULE;
	if (cdev_add(&(bbbi2c_dev_ptr->cdev), bbbi2c_dev_no,1) != 0) {
		driver_err("%s: Cannot add device\n", DEVICE_NAME);
		goto failed_add_device;
	}
	bbbi2c_dev_ptr->dev_ptr=device_create (bbbi2cclass_ptr, NULL, MKDEV(MAJOR(bbbi2c_dev_no), 0), NULL, DEVICE_PROCESS, 0);
	if (IS_ERR(bbbi2c_dev_ptr->dev_ptr)){
		driver_err("%s: Cannot create device\n", DEVICE_NAME);
		goto failed_device_create;
	}
	driver_info ("%s: Registered device: (%d,%d)\n", DEVICE_NAME, MAJOR(bbbi2c_dev_no), MINOR(bbbi2c_dev_no));
	/* driver_info ("%s: Driver is loaded: ver. %d, built on %s, debug=%d\n", DEVICE_NAME, DRIVER_VERSION, __DATE__ " " __TIME__ , debug); */
	driver_info ("%s: Driver is loaded: ver. %d, debug=%d\n", DEVICE_NAME, DRIVER_VERSION, debug);
	return 0;

failed_device_create:
	{
		device_destroy(bbbi2cclass_ptr, MKDEV (MAJOR (bbbi2c_dev_no), 0));
		cdev_del(&(bbbi2c_dev_ptr->cdev));
	}
failed_add_device:
	{
		class_destroy (bbbi2cclass_ptr);
		bbbi2cclass_ptr = NULL;
	}
failed_class_create:
	{
		unregister_chrdev_region (bbbi2c_dev_no, 1);
	}
failed_register:
	{
		kfree(bbbi2c_dev_ptr);
		bbbi2c_dev_ptr = NULL;
	}
failed_alloc:
	{
		return -EBUSY;
	}
}

static void __exit bbbi2c_exit(void)
{
		// for compatibility with .remove function
		//struct adnp *adnp = i2c_get_clientdata(client);

        driver_info("%s: Unregistering...\n", DEVICE_NAME);
        if (bbbi2c_dev_ptr != NULL) {
                device_destroy (bbbi2cclass_ptr, MKDEV(MAJOR(bbbi2c_dev_no), 0));
                cdev_del(&(bbbi2c_dev_ptr->cdev));
                kfree(bbbi2c_dev_ptr);
                bbbi2c_dev_ptr = NULL;
        }
        unregister_chrdev_region(bbbi2c_dev_no, 1);
        if (bbbi2cclass_ptr != NULL) {
                class_destroy (bbbi2cclass_ptr);
                bbbi2cclass_ptr = NULL;
        }
        driver_info("%s: Driver unloaded\n", DEVICE_NAME);
}

static const struct i2c_device_id hycon_hy46xx_id[] = {
	{ .name = "hy4613" },
	{ .name = "hy4614" },
	{ .name = "hy4621" },
	{ .name = "hy4623" },
	{ .name = "hy4633" },
	{ .name = "hy4635" },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(i2c, hycon_hy46xx_id);

static const struct of_device_id hycon_hy46xx_of_match[] = {
	{ .compatible = "hycon,hy4613" },
	{ .compatible = "hycon,hy4614" },
	{ .compatible = "hycon,hy4621" },
	{ .compatible = "hycon,hy4623" },
	{ .compatible = "hycon,hy4633" },
	{ .compatible = "hycon,hy4635" },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, hycon_hy46xx_of_match);

static struct i2c_driver hycon_hy46xx_driver = {
	.driver = {
		.name = "hycon_hy46xx",
		.owner	= THIS_MODULE,
		.of_match_table = hycon_hy46xx_of_match,
		//.probe_type = PROBE_PREFER_ASYNCHRONOUS,
	},
	.id_table = hycon_hy46xx_id,
	.probe    = hycon_hy46xx_probe,
};

module_i2c_driver(hycon_hy46xx_driver);
module_init (bbbi2c_init);
module_exit (bbbi2c_exit);

MODULE_AUTHOR("Giulio Benetti <giulio.benetti@benettiengineering.com>");
MODULE_DESCRIPTION("HYCON HY46XX I2C Touchscreen Driver");
MODULE_LICENSE("GPL v2");
MODULE_VERSION("0.98");
MODULE_ALIAS("platform:hycon_hy46xx");
