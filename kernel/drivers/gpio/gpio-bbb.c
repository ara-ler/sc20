/*
Copyright 2020 (c) Apergy Corp. <sergey.manucharian@apergy.com>
Copyright 2015 (c) 23ars <ardeleanasm@gmail.com>

--------------------------------------------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. See "gpl.txt" file.
If not received, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------

This is a fork of the driver written by 23ars <ardeleanasm@gmail.com>

  by_SM:
  This code may be useful too:
  https://github.com/ardeleanasm/input_capture_driver

This is a driver for BeagleBone Black GPIOs.
It allows to set up pins' direction, read/write values and enable interrupts.

The interrupts capture functionality is the main purpose of this project.
In contrast of the traditional poll() and epoll() mechanisms,
which consume valuable resources of the BBB's CPU), this driver simply sends
a real-time signal to the user application when an interrupt request occurs.
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/reboot.h>
#include <linux/pid_namespace.h>
#include <linux/pid.h>
#include <linux/time64.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/sched/signal.h>
#include <linux/spi/spi.h>
#include <linux/spi/spidev.h>

#include <linux/gpio/consumer.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>
#include "bbbgpio_ioctl.h"

/* This is from kernel defines */
/*
#define KERN_SOH        "\001"          // ASCII Start Of Header
#define KERN_EMERG      KERN_SOH ""     // system is unusable
#define KERN_ALERT      KERN_SOH "1"    // action must be taken immediately
#define KERN_CRIT       KERN_SOH "2"    // critical conditions
#define KERN_ERR        KERN_SOH "3"    // error conditions
#define KERN_WARNING    KERN_SOH "4"    // warning conditions
#define KERN_NOTICE     KERN_SOH "5"    // normal but significant condition
#define KERN_INFO       KERN_SOH "6"    // informational
#define KERN_DEBUG      KERN_SOH "7"    // debug-level messages
#define KERN_DEFAULT    KERN_SOH "d"    // the default kernel loglevel
*/

/*====================================
  DRIVER's INFO
====================================*/

#define DRIVER_AUTHOR "Apergy corporation <sergey.manucharian@apergy.com>"
#define DRIVER_DESC "GPIO driver for BBB"
#define DEVICE_NAME "bbbgpio"
#define DEVICE_CLASS_NAME "bbbgpio_class"
#define DEVICE_PROCESS "bbbgpio%d"

/* YY DDD (year and Julian date as version) */
#define DRIVER_VERSION	22012
#define MAX_GPIO		127
#define VIRT_GPIO		255

/*==================
  DEBUGGING MACROS
===================*/

#define driver_info(format,arg...)	do { printk( KERN_INFO format, ## arg ); } while(0)
#define driver_err(format,arg...)	do { printk( KERN_ERR  format, ## arg ); } while(0)

#define TMR_INTERVAL_DEF	5000000
#define DEBOUNCE_MS_DEF		2

/* Use a real-time signal to send to user upon irq */
int signal_no = SIGRT35;

/* bbbgpio device structure */
struct bbbgpio_device {
	struct cdev cdev;
	struct device *dev_ptr;
	u8 is_open;
};

struct irq_gpio {
	int gpio;	
	int irq_level;	
	uint64_t irq_count;
	uint64_t irq_gross_count;
	uint8_t signal;
	uint32_t debounce_ms;
	int valid_tmr_ival;
	int tmr_count;
	struct timespec64 timestamp;
};

enum bbbgpio_direction
{
	INPUT=0x00,
	OUTPUT
};

static struct 			bbbgpio_device *bbbgpiodev_ptr = NULL;
static dev_t 			bbbgpio_dev_no;
static struct 			class *bbbgpioclass_ptr = NULL;
static struct 			bbb_irq_counter_currtime irq_counter_currtime;
static unsigned long 	irq_flags = IRQF_TRIGGER_NONE;
static volatile int 	bbb_irq = -1;
struct task_struct 		*usr_task = NULL;
struct task_struct 		*usr_task_aux = NULL;
static struct hrtimer	dig_timer;
static ktime_t 			ktime;

static long 	error_code;
static int 		debug = 0;				/* debug flag to enable/disable extra messages via printk() */
/* static uint32_t	irq_glob_count = 0;		 global counter for gpio irqs */
static uint32_t	tmr_glob_count = 0;		 /* global counter for digitization timer */
static bool 	print_stats = false;
static int 		sig_on = 1;				/* enable/disable sending signal to user application */
static unsigned long	dig_tmr_period = 0;		/* pulse digitization timer period in ns */
static bool		dig_tmr_set = false;	/* pulse digitization timer is set */
static int 		max_int_idx = 0;		/* maximum index of the registered interrupt pins */
static int 		drv_version = DRIVER_VERSION;
static char 	kern_debug[4];

static struct 	irq_gpio int_pins[MAX_INT_PINS];
static unsigned int	uncounted_irq = 0;

static int 		bbbgpio_open (struct inode*, struct file*);
static int 		bbbgpio_release (struct inode*, struct file*);

static long 	bbbgpio_ioctl (struct file*, unsigned int ,unsigned long );
static ssize_t 	bbbgpio_read (struct file *, char __user*,size_t, loff_t*);
static ssize_t 	bbbgpio_write (struct file *, const char __user *, size_t, loff_t *);
static 			irq_handler_t interrupt_handler (int, void *, struct pt_regs *);
static void		release_all_ints (void);

struct 			file_operations fops =
{
	.open = bbbgpio_open,
	.release = bbbgpio_release,
	.unlocked_ioctl = bbbgpio_ioctl,
	.read = bbbgpio_read,
	.write = bbbgpio_write
};

enum hrtimer_restart timer_callback (struct hrtimer *timer);

static int 
bbbgpio_open(struct inode *inode,struct file *file)
{
	bbbgpiodev_ptr->is_open = 1;
	printk ("%s %s: Device opened\n", kern_debug, DEVICE_NAME);
	return 0;     
}

static void
release_all_ints(void) 
{
	int i;

	/* release all interrupt handlers */
	for (i = 0; (i < MAX_INT_PINS && int_pins[i].gpio != -1); i++) {
		bbb_irq = gpio_to_irq (int_pins[i].gpio);
		free_irq (bbb_irq, (void *)(unsigned long)int_pins[i].gpio);
		printk ("%s %s: Interrupt for pin %d disabled\n", kern_debug, DEVICE_NAME, int_pins[i].gpio);
		int_pins[i].gpio = -1;
	}
}

static void
release_gpios (void)
{
	release_all_ints();
	if (dig_tmr_set) {
		if (0 != debug) driver_err("%s: release_gpios(): trying to delete timer\n", DEVICE_NAME);
		if (hrtimer_cancel(&dig_timer))
			driver_err("%s: release_gpios(): cannot delete timer\n", DEVICE_NAME);
		else
			dig_tmr_set = false;
	}
}


static int
bbbgpio_release(struct inode *inode, struct file *file)
{
	release_gpios();
	printk("%s %s: Device is closed\n", kern_debug, DEVICE_NAME);
	bbbgpiodev_ptr->is_open = 0;
	return 0;
}

static int
dig_timer_init (unsigned long tmr_interval)
{
	dig_tmr_period = tmr_interval;
	if (0 != debug) driver_err("%s: dig_timer_init(%ld)\n", DEVICE_NAME, dig_tmr_period);
	if (!dig_tmr_set && dig_tmr_period > 0) {
			dig_tmr_set = true;
			if (0 != debug) driver_err("%s: dig_timer_init(): trying to setup timer\n", DEVICE_NAME);
			ktime = ktime_set(0, tmr_interval);
			hrtimer_init(&dig_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
			dig_timer.function = &timer_callback;
			hrtimer_start(&dig_timer, ktime, HRTIMER_MODE_REL);
			driver_err("%s: dig_timer_init(): timer set up with interval %ld ns\n", DEVICE_NAME, dig_tmr_period);
			return 0;
	}
	return 2;
}

static long 
bbbgpio_ioctl (struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
	int i;
	unsigned int port_pin;
	int usr_pid = 0;	
	struct timespec64 sys_time;
	struct bbb_ioctl_struct ioctl_buffer;

	// spi stuff
	uint32_t spi_mode = 0;
	struct file* fd;
	mm_segment_t old_fs;
    struct spi_ioc_transfer spi_xfer;
	char tx_buffer[4] = {0xA, 0x3, 0x5, 0x6}; 
	char rx_buffer[4];

	memset (&ioctl_buffer, 0, sizeof(struct bbb_ioctl_struct));

	if (bbbgpiodev_ptr == NULL) {
		driver_err ("%s: Device not found!\n", DEVICE_NAME);
		return -ENODEV;
	}
	/* check if we need bbb_ioctl_struct, if yes, copy it over */
	if (ioctl_num <= PIN_MAX)
		if (copy_from_user (&ioctl_buffer, (void *)ioctl_param, sizeof(struct bbb_ioctl_struct)) != 0) {
			driver_err ("%s: Could not copy data from userspace!\n", DEVICE_NAME);
			return -EINVAL;
		}
	switch (ioctl_num) {

		case PIN_REG:
			if (0 != debug) driver_err("%s: PIN_REG: %u\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			if (ioctl_buffer.pin_or_port_num > MAX_GPIO) {
				driver_err ("%s: PIN_REG: cannot register pin, pin %u does not exist\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
				return -EINVAL;
			}
			error_code = gpio_request(ioctl_buffer.pin_or_port_num, "bbb_gpio");
			if (error_code != 0 && error_code != -EBUSY) {
				driver_err ("%s: PIN_REG: cannot register pin, error = %ld\n", DEVICE_NAME, error_code);
				return error_code;
			}
			break;

		case PIN_UNREG:
			if (0 != debug) driver_err("%s: PIN_UNREG: %u\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			gpio_unexport(ioctl_buffer.pin_or_port_num);
			gpio_free(ioctl_buffer.pin_or_port_num);
			break;

		case PIN_RESERVED:
			break;

		case PIN_SETDIR:
			if (0 != debug) driver_err("%s: PIN_SETDIR: %u dir=%u\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num, ioctl_buffer.value);
			if (ioctl_buffer.value == OUTPUT)
				error_code = gpio_direction_output (ioctl_buffer.pin_or_port_num, 0);
			else 
				error_code = gpio_direction_input (ioctl_buffer.pin_or_port_num);
			
			if (error_code != 0) {
				driver_err ("%s: PIN_SETDIR: cannot set pin direction, error = %ld\n", DEVICE_NAME, error_code);
				return error_code;
			}

			gpio_export (ioctl_buffer.pin_or_port_num, false);
			break;

		case PIN_RISEDGE:
			irq_flags = IRQF_TRIGGER_RISING;
			sig_on = 1;
			if (0 != debug) driver_err ("%s: PIN_RISEDGE: sigon=%d\n", DEVICE_NAME, sig_on);
			break; 

		case PIN_FALEDGE:
			irq_flags = IRQF_TRIGGER_FALLING;
			sig_on = 1;
			if (0 != debug) driver_err ("%s: PIN_FALEDGE: sigon=%d\n", DEVICE_NAME, sig_on);
			break; 

		case PIN_RISEDGE_NO_SIGNAL:
			irq_flags = IRQF_TRIGGER_RISING;
			sig_on = 0;		/* don't send signals, just count interrupts */
			if (0 != debug) driver_err ("%s: PIN_RISEDGE_NO_SIGNAL: sigon=%d\n", DEVICE_NAME, sig_on);
			break;

		case PIN_FALEDGE_NO_SIGNAL:
			irq_flags = IRQF_TRIGGER_FALLING;
			sig_on = 0;		/* don't send signals, just count interrupts */
			if (0 != debug) driver_err ("%s: PIN_FALEDGE_NO_SIGNAL: sigon=%d\n", DEVICE_NAME, sig_on);
			break;

		case PIN_ENINT:
			if (0 != debug) driver_err ("%s: PIN_ENINT: trying pin=%u...\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			bbb_irq = 0;
			/* check whether that pin interrupt is already enabled */
			for (i = 0; (i < MAX_INT_PINS && int_pins[i].gpio != -1); i++) {
				if (int_pins[i].gpio == ioctl_buffer.pin_or_port_num) {
					driver_err ("%s: PIN_ENINT: interrupt handler for pin %d is already set!\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
					bbb_irq = -1;
				}
			}
			if (bbb_irq != -1) {
				bbb_irq = gpio_to_irq (ioctl_buffer.pin_or_port_num);
				/* let's try to free that irq in case of residual configuration */
				/* ...it's not happy to free an already free irq for some reason... */
				/* free_irq (bbb_irq, (void *)(unsigned long)ioctl_buffer.pin_or_port_num); */
				if (request_irq (bbb_irq, (irq_handler_t)interrupt_handler, irq_flags,
								DEVICE_NAME, (void *)(unsigned long)ioctl_buffer.pin_or_port_num)) {
					driver_err ("%s: Cannot assign irq %d to pin %d!\n", DEVICE_NAME, bbb_irq, ioctl_buffer.pin_or_port_num);
					bbb_irq = -1;
				}
			}
			if (bbb_irq != -1) {
				/* store record of the pin */
				int_pins[i].gpio = ioctl_buffer.pin_or_port_num; 
				int_pins[i].irq_level = irq_flags == IRQF_TRIGGER_RISING ? 1 : 0;
				max_int_idx++;
				int_pins[i].irq_count = 0;
				int_pins[i].irq_gross_count = 0;
				int_pins[i].timestamp.tv_sec = 0;
				int_pins[i].timestamp.tv_nsec = 0;
				int_pins[i].signal = sig_on;
				int_pins[i].debounce_ms = ioctl_buffer.value;

				printk ("%s %s: int_pins[%d].gpio = %d, irq_flags=%ld (↑0x01,↓0x10), sigon=%d, debounce=%u ms\n", kern_debug, DEVICE_NAME, i, int_pins[i].gpio, irq_flags, int_pins[i].signal, int_pins[i].debounce_ms);
				ioctl_buffer.value = bbb_irq;
				if (copy_to_user((void *)ioctl_param, &ioctl_buffer, sizeof(struct bbb_ioctl_struct)) != 0) {
					driver_err("%s: PIN_ENINT: cannot send data to user!\n", DEVICE_NAME);
				}
			}
			else
				return -EINVAL;

			break;

		case PIN_DISINT:
			if (0 != debug) driver_err ("%s: PIN_DISINT: trying pin=%u...\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			/* clear record of the pin and shift other records */
			for (i = 0; (i < MAX_INT_PINS - 1 && int_pins[i].gpio != ioctl_buffer.pin_or_port_num && int_pins[i].gpio != -1); i++) {}
			if (int_pins[i].gpio == ioctl_buffer.pin_or_port_num) {
				printk ("%s %s: int_pins[%d]gpio = %d cleared\n", kern_debug, DEVICE_NAME, i, int_pins[i].gpio);
				bbb_irq = gpio_to_irq (ioctl_buffer.pin_or_port_num);
				free_irq (bbb_irq, (void *)(unsigned long)ioctl_buffer.pin_or_port_num);
				printk ("%s %s: PIN_DISINT: disable interrupt %d on pin %d\n", kern_debug, DEVICE_NAME, bbb_irq, ioctl_buffer.pin_or_port_num);
				for (; (int_pins[i].gpio != -1 && i < MAX_INT_PINS - 1); i++) {
					int_pins[i].gpio = int_pins[i+1].gpio;
				}
				int_pins[MAX_INT_PINS - 1].gpio = -1;	
				max_int_idx--;
			}
			else {
				printk ("%s %s: PIN_DISINT: cannot find record for interrupt on pin %d\n", kern_debug, DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			}

			break;

		case PIN_RES_IRQCOUNT:
			if (0 != debug) driver_err ("%s: PIN_RES_IRQCOUNT: trying pin=%u...\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			for (i = 0; (i < MAX_INT_PINS - 1 && int_pins[i].gpio != ioctl_buffer.pin_or_port_num && int_pins[i].gpio != -1); i++) {}
			if (int_pins[i].gpio == ioctl_buffer.pin_or_port_num) {
				printk ("%s %s: irq_count for int_pins[%d]gpio = %d\n", kern_debug, DEVICE_NAME, i, int_pins[i].gpio);
				bbb_irq = gpio_to_irq (ioctl_buffer.pin_or_port_num);
				if (bbb_irq != -1) {
					int_pins[i].irq_count = 0;
					int_pins[i].irq_gross_count = 0;
					int_pins[i].timestamp.tv_sec = 0;
					int_pins[i].timestamp.tv_nsec = 0;
				}
				else {
					printk ("%s %s: PIN_RES_IRQCOUNT: cannot convert pin %d to IRQ number\n", kern_debug, DEVICE_NAME, ioctl_buffer.pin_or_port_num);
				}
			}
			else {
				printk ("%s %s: PIN_RES_IRQCOUNT: cannot find record for interrupt on pin %d\n", kern_debug, DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			}

			break;

		case PIN_READ_ONE_PORT:
			if (0 != debug) driver_err ("%s: PIN_READ_ONE_PORT: port=%u\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			if (ioctl_buffer.pin_or_port_num > 3) {
				if (0 != debug) driver_err ("%s: PIN_READ_ONE_PORT: port=%u is invalid!\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
				return -EINVAL;
			}
			ioctl_buffer.value = 0;
			port_pin = 32 * ioctl_buffer.pin_or_port_num;
			if (0 != debug) driver_err ("%s: PIN_READ_ONE_PORT: base pin number: %u\n", DEVICE_NAME, port_pin);
			for (i = 31; i >= 0 ; i--) {
				ioctl_buffer.value <<= 1;
				ioctl_buffer.value |= gpio_get_value (port_pin + i);
			}
			if (copy_to_user((void *)ioctl_param, &ioctl_buffer, sizeof(struct bbb_ioctl_struct)) != 0) {
				driver_err("\t%s: PIN_READ_ONE_PORT: cannot send data to userspace!\n", DEVICE_NAME);
				return -EIO;
			}
			break;

		/* currently the validity period is set in PIN_ENINT, although this IOCTL can be used too */
		case PIN_VALID_PERIOD:
			if (0 != debug) driver_err ("%s: PIN_VALID_PERIOD: pin=%u\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);
			/* check whether that pin interrupt is already enabled */
			for (i = 0; (i < MAX_INT_PINS && int_pins[i].gpio != -1); i++) {
				if (int_pins[i].gpio == ioctl_buffer.pin_or_port_num) {
					if (dig_tmr_set) {
						int_pins[i].valid_tmr_ival = ioctl_buffer.value;
						driver_err ("%s: PIN_VALID_PERIOD: setting valid_tmr_ival=%d for pin %d (%d)\n", DEVICE_NAME, int_pins[i].valid_tmr_ival, int_pins[i].gpio, i);
					}
					else {
						int_pins[i].debounce_ms = ioctl_buffer.value;
						driver_err ("%s: PIN_VALID_PERIOD: setting debounce_ms=%d for pin %d (%d)\n", DEVICE_NAME, int_pins[i].debounce_ms, int_pins[i].gpio, i);
					}

					break;
				}
			}
			if (i == MAX_INT_PINS)
				driver_err ("%s: PIN_VALID_PERIOD: pin %d is not enabled for interrupts!\n", DEVICE_NAME, ioctl_buffer.pin_or_port_num);

			break;

		case VAL_SENDPID:
			if (copy_from_user(&usr_pid, (int *)ioctl_param, sizeof(int))) {
				driver_err ("%s: Cannot not get PID from user!\n", DEVICE_NAME);
			}
			printk ("%s %s: Got PID=%d, will send IRQ to it\n", kern_debug, DEVICE_NAME, usr_pid);
			rcu_read_lock();
			usr_task = pid_task (find_vpid (usr_pid), PIDTYPE_PID);
			if (usr_task == NULL)
				driver_err ("%s: Cannot not find user task from PID=%d\n", DEVICE_NAME, usr_pid);
			rcu_read_unlock();
			break;

		case VAL_SENDPID_AUX:
			if (copy_from_user(&usr_pid, (int *)ioctl_param, sizeof(int))) {
				driver_err ("%s: Cannot not get PID from user!\n", DEVICE_NAME);
			}
			printk ("%s %s: Got PID=%d, will send IRQ to it\n", kern_debug, DEVICE_NAME, usr_pid);
			rcu_read_lock();
			usr_task_aux = pid_task (find_vpid (usr_pid), PIDTYPE_PID);
			if (usr_task_aux == NULL)
				driver_err ("%s: Cannot not find user task from PID=%d\n", DEVICE_NAME, usr_pid);
			rcu_read_unlock();
			break;

		case VAL_GETSIG:
			if (copy_to_user ((void *)ioctl_param, &signal_no, sizeof(int)) != 0) {
				driver_err("%s: VAL_GETSIG: cannot send data to user!\n", DEVICE_NAME);
				return -EIO;
			}
			printk ("%s %s: Signal number used: %d (set by driver)\n", kern_debug, DEVICE_NAME, signal_no);

			break;

		case VAL_SETSIG:
			if (copy_from_user(&signal_no, (int *)ioctl_param, sizeof(int))) {
				driver_err("%s: Cannot not get PID from user!\n", DEVICE_NAME);
			}
			printk ("%s %s: Signal number used: %d (received from user)\n", kern_debug, DEVICE_NAME, signal_no);

			break;

		case VAL_RELINTS:
			release_all_ints();

			break;

		case VAL_GETINTS:	/* for debugging */
			if (copy_to_user((void*)ioctl_param, int_pins, sizeof(int_pins)) != 0) {
				driver_err("%s: VAL_RELINTS: cannot send data to user!\n", DEVICE_NAME);
				return -EIO;
			}

			break;

		case VAL_GET_UNCOUNTED:
			if (copy_to_user((void*)ioctl_param, &uncounted_irq, sizeof(uncounted_irq)) != 0) {
				driver_err("%s: VAL_GET_UNCOUNTED: cannot send data to user!\n", DEVICE_NAME);
				return -EIO;
			}

			break;

		case VAL_DRVVER:
			if (copy_to_user((void*)ioctl_param, &drv_version, sizeof(int)) != 0) {
				driver_err("%s: VAL_DRVVER: cannot send driver version to user!\n", DEVICE_NAME);
				return -EIO;
			}
			else {
				driver_info("%s: Driver version = %d\n", DEVICE_NAME, drv_version);
			}

			break;

		case VAL_DEBUG:
			if (copy_from_user(&debug, (int *)ioctl_param, sizeof(int))) {
				driver_err("%s: VAL_DEBUG: cannot copy debug setting from user!\n", DEVICE_NAME);
				return -EINVAL;
			}
			if (0 != debug) {
				strcpy (kern_debug, KERN_SOH "3");
				driver_err ("%s: debugging messages enabled\n", DEVICE_NAME);
			}
			else
				strcpy (kern_debug, KERN_SOH "d");

			break;

		case VAL_SIGONOFF:
			if (copy_from_user(&sig_on, (int *)ioctl_param, sizeof(int))) {
				driver_err("%s: VAL_SIGONOFF: cannot copy setting from user!\n", DEVICE_NAME);
				return -EINVAL;
			}

			break;

		case VAL_TIMER_INTERVAL:
			if (copy_from_user(&dig_tmr_period, (int *)ioctl_param, sizeof(int))) {
				driver_err("%s: VAL_TIMER_INTERVAL: cannot copy setting from user!\n", DEVICE_NAME);
				return -EINVAL;
			}
			dig_timer_init (dig_tmr_period);

			break;

		case SYS_TIME:
			if (copy_from_user(&sys_time.tv_sec, (time64_t *)ioctl_param, sizeof(time64_t))) {
				driver_err("%s: Cannot not get system time from user!\n", DEVICE_NAME);
				return -EINVAL;
			}
			printk ("%s %s: Trying to set system time to %lld seconds\n", kern_debug, DEVICE_NAME, sys_time.tv_sec);
			sys_time.tv_nsec = 0;
			error_code = do_settimeofday64(&sys_time);
			if (error_code !=0)
				return error_code;

			break;

		case GET_IRQ_COUNT_CURRTIME:
			if (copy_from_user(&irq_counter_currtime, (struct bbb_irq_counter_currtime *)ioctl_param, sizeof(struct bbb_irq_counter_currtime))) {
				driver_err("%s: Cannot not get iqr counter structure from user!\n", DEVICE_NAME);
				return -EINVAL;
			}

			for (i = 0; (i < max_int_idx && int_pins[i].gpio != -1); i++) {
				if (irq_counter_currtime.pin_in_ms_out == int_pins[i].gpio) {
					if (0 != debug) driver_err("%s: searching for pin counter, pin=%lld, idx=%d\n", DEVICE_NAME, irq_counter_currtime.pin_in_ms_out, i);
					irq_counter_currtime.pulse_count = int_pins[i].irq_count;

					irq_counter_currtime.pin_in_ms_out = (uint64_t)((uint64_t)int_pins[i].timestamp.tv_sec * 1000 + int_pins[i].timestamp.tv_nsec / 1000000);
					ktime_get_real_ts64(&sys_time);
					irq_counter_currtime.currtime_in_ms_out = (uint64_t)((uint64_t)sys_time.tv_sec * 1000 + sys_time.tv_nsec / 1000000);
					if (0 != debug) {
						driver_err("%s: last pulse: sec=%lld, nsec=%ld\n", DEVICE_NAME, int_pins[i].timestamp.tv_sec, int_pins[i].timestamp.tv_nsec);
						driver_err("%s: curr. time: sec=%lld, nsec=%ld\n", DEVICE_NAME, sys_time.tv_sec, sys_time.tv_nsec);
					}
					i = 0;
					break;
				}
			}
			if (0 != i) {
				return -EINVAL;
			}
			if (copy_to_user((void *)ioctl_param, &irq_counter_currtime, sizeof(struct bbb_irq_counter_currtime)) != 0) {
				driver_err("%s: GET_IRQ_COUNTER_CURRTIME: cannot send data to user!\n", DEVICE_NAME);
				return -EIO;
			}

			break;

		case SEND_SPI_MSG:
			// FIXME This can be done directly with
			// spi_message_add_tail(&xfer, &msg);
			// spi_sync(spi, &msg);

			spi_mode |= SPI_CPHA;
            spi_mode |= SPI_CPOL;


			memset(&spi_xfer, 0, sizeof(spi_xfer));
			spi_xfer.tx_buf = (unsigned long)tx_buffer;
			spi_xfer.rx_buf = (unsigned long)rx_buffer;
			spi_xfer.speed_hz = 1000000;
			spi_xfer.len = 1;

			/* Open file (in kernel space it shouldn't be used) */
			fd = filp_open("/dev/spidev1.0", O_RDWR, 0);
			// Replace user space with kernel space
			old_fs = get_fs();
			set_fs(KERNEL_DS);

			fd->f_op->unlocked_ioctl(fd, SPI_IOC_WR_MODE32, (unsigned long)&spi_mode);
			fd->f_op->unlocked_ioctl(fd, SPI_IOC_MESSAGE (1), (unsigned long)&spi_xfer);

			// Restore space
			set_fs(old_fs);
			filp_close(fd, 0);

			break;

		case DISABLE_ALLINTS:
			release_all_ints(); 

			break;

		case EMERGENCY_RESTART:
			emergency_restart();
			break;

		default:
			return -ENOTTY;
	}
	return 0;     
}

static ssize_t 
bbbgpio_read (struct file *filp, char __user *buffer, size_t length, loff_t *offset)
{
	struct bbb_ioctl_struct ioctl_buffer_read;
	//driver_err ("%s: Read op", DEVICE_NAME);

	if (copy_from_user (&ioctl_buffer_read, buffer, sizeof(struct bbb_ioctl_struct)) != 0) {
		driver_err ("%s: Read operation: cannot not copy data from userspace!\n", DEVICE_NAME);
		return -EINVAL;
	}
	
	if (ioctl_buffer_read.pin_or_port_num == VIRT_GPIO) 
		driver_err ("%s: Virtual pin %d: reading value %d\n", DEVICE_NAME, VIRT_GPIO, ioctl_buffer_read.value);
	else
		ioctl_buffer_read.value = gpio_get_value (ioctl_buffer_read.pin_or_port_num);

	if (copy_to_user (buffer, &ioctl_buffer_read, sizeof(struct bbb_ioctl_struct)) !=0 ) {
		driver_err("\t%s: Read operation: cannot send data to userspace!\n", DEVICE_NAME);
		return -EIO;
	}
	return 0;
	
}
static ssize_t 
bbbgpio_write (struct file *filp, const char __user *buffer, size_t length, loff_t *offset)
{
	struct bbb_ioctl_struct ioctl_buffer_write;
	struct siginfo info;
	int rc;
	int int_bitmask = 0;

	//driver_err ("%s: Write op", DEVICE_NAME);
	if (copy_from_user (&ioctl_buffer_write, buffer, sizeof(struct bbb_ioctl_struct)) != 0) {
		driver_err ("%s: Write operation: cannot copy data from userspace!\n", DEVICE_NAME);
		return -EINVAL;
	}
	/* This is to send a signal to a program when VIRT_GPIO is referenced */
	if (ioctl_buffer_write.pin_or_port_num == VIRT_GPIO) {
		driver_err ("%s: Virtual pin %d: writing value %d\n", DEVICE_NAME, VIRT_GPIO, ioctl_buffer_write.value);
		if (NULL != usr_task_aux) { 
			info.si_errno = 0;
			info.si_signo = signal_no;
			/* info.si_code = SI_KERNEL ; */
			info.si_code = SI_QUEUE; 
			info.si_int = int_bitmask;
			rc = send_sig_info (signal_no, &info, usr_task_aux);
			if (rc < 0) {
				driver_err ("%s: Error sending signal!\n", DEVICE_NAME);
			}
		}
		else {
			driver_err ("%s: User task struct is NULL, cannot send signal to\n", DEVICE_NAME);
		}
	}
	else
		gpio_set_value(ioctl_buffer_write.pin_or_port_num, ioctl_buffer_write.value);
	return 0;
}

static irq_handler_t 
interrupt_handler (int irq, void *dev_id, struct pt_regs *regs)
{
	u16 io_number = (u16)(unsigned long)dev_id;	/* the actual pin the interrupt occured at */
	int i;
	int rc;
	bool pulse_valid = false;
	int int_bitmask = 0;
	uint32_t pulse_interval;
	struct siginfo info;
	struct timespec64 time_now;

	ktime_get_real_ts64(&time_now);

	/* irq_glob_count++; */

	/* FIXME! the bitmask mechanism is kept to avoid breaking certain userspace applications */
	for (i = 0; (i < max_int_idx && int_pins[i].gpio != -1); i++) {
		if (io_number == int_pins[i].gpio) {
			int_pins[i].irq_gross_count++;
			if (!dig_tmr_set) {			/* i.e. using squelch */
				pulse_interval = 	(uint64_t)((uint64_t)             time_now.tv_sec * 1000 +              time_now.tv_nsec / 1000000) - 
									(uint64_t)((uint64_t)int_pins[i].timestamp.tv_sec * 1000 + int_pins[i].timestamp.tv_nsec / 1000000);
				if (pulse_interval >= int_pins[i].debounce_ms) {
					pulse_valid = true;
				}
				/* we don't want to flood dmesg with this:
				else {
					driver_err ("%s: pulse on pin=%d squelched since Δ=%u < %u\n", DEVICE_NAME, int_pins[i].gpio, pulse_interval, int_pins[i].debounce_ms); 
				}
				*/
			}
			else {
				if (dig_tmr_period !=0 && int_pins[i].tmr_count >= int_pins[i].valid_tmr_ival) {
					pulse_valid = true;
					if (0 != debug) driver_err ("%s: tmr_count=%d valid_tmr_ival=%d\n", DEVICE_NAME, int_pins[i].tmr_count, int_pins[i].valid_tmr_ival); 
				}
			}
			if (print_stats && 3 == debug) {
				driver_err ("%s: ticks=%d, pin %d irq count: gross=%lld, valid=%lld\n", DEVICE_NAME, tmr_glob_count, int_pins[i].gpio, int_pins[i].irq_gross_count, int_pins[i].irq_count); 
				print_stats = false;
			}
			break;
		}
	}
	if (i == max_int_idx)
		return (irq_handler_t) IRQ_HANDLED;
		

	if (pulse_valid) {
		int_pins[i].tmr_count = 0; 
		int_pins[i].timestamp.tv_sec = time_now.tv_sec;
		int_pins[i].timestamp.tv_nsec = time_now.tv_nsec;
		int_pins[i].irq_count++;
		int_bitmask |= (1 << i);
		if (0 != debug)	driver_err ("%s: IRQ on pin=%d SIG%d bitmask=0x%02X gpio=%d (%d) sigon=%d count=%lld\n", DEVICE_NAME, io_number, signal_no, info.si_int, int_pins[i].gpio, i, int_pins[i].signal, int_pins[i].irq_count);
		if (0 != int_pins[i].signal) {
			if (NULL != usr_task) { 
				info.si_errno = 0;
				info.si_signo = signal_no;
				/* info.si_code = SI_KERNEL ; */
				info.si_code = SI_QUEUE; 
				info.si_int = int_bitmask;
				rc = send_sig_info (signal_no, &info, usr_task);
				if (rc < 0) {
					driver_err ("%s: Error sending signal!\n", DEVICE_NAME);
				}
			}
			else {
				driver_err ("%s: User task struct is NULL, cannot send signal to\n", DEVICE_NAME);
			}
		}
	}
	else	/* count the irq anyway for diagostics/testing purposes */
		uncounted_irq++;

	return (irq_handler_t) IRQ_HANDLED;
}

/* Currently is not in use - the old squelch is in effect */
enum hrtimer_restart
timer_callback (struct hrtimer *timer)
{
	int i;

	tmr_glob_count++; 
	if (0 == tmr_glob_count%200) {
		/* driver_err ("%s: tmr_glob_count=%d\n", DEVICE_NAME, tmr_glob_count); */
		print_stats = true;
	}

	if (debug >= 2) driver_err("%s: timer_callback() entry point\n", DEVICE_NAME);
	if (dig_tmr_set) {
		if (debug >= 2)	driver_err ("%s: timer_callback() check\n", DEVICE_NAME);
		for (i = 0; (i < max_int_idx && int_pins[i].gpio != -1); i++) {
			if (int_pins[i].valid_tmr_ival != 0) {
				if (int_pins[i].irq_level != gpio_get_value(int_pins[i].gpio)) {
					int_pins[i].tmr_count++;
				}
				else
					int_pins[i].tmr_count = 0;
			}
		}
		hrtimer_forward(&dig_timer, ktime_get(), ktime);
		return HRTIMER_RESTART;
	}
	return HRTIMER_NORESTART;
}

static int
__init bbbgpio_init(void)
{
	int j;
	/* keep records on interrupt enabled pins to release them all when needed */
	for (j = 0 ; j < MAX_INT_PINS ; j++) {
		int_pins[j].gpio = -1;
	}

	bbbgpiodev_ptr = kmalloc(sizeof(struct bbbgpio_device),GFP_KERNEL);
	if (bbbgpiodev_ptr == NULL) {
		driver_err ("%s: Failed to allocate memory for bbbgpiodev_ptr\n", DEVICE_NAME);
		goto failed_alloc;
	}
	memset(bbbgpiodev_ptr, 0,sizeof(struct bbbgpio_device));
	if (alloc_chrdev_region(&bbbgpio_dev_no, 0, 1, DEVICE_NAME) < 0) {
		driver_err ("%s: Cannot register\n", DEVICE_NAME);
		goto failed_register;
	}
	bbbgpioclass_ptr=class_create(THIS_MODULE, DEVICE_CLASS_NAME);
	if (IS_ERR(bbbgpioclass_ptr)) {
		driver_err ("%s: Cannot create class\n", DEVICE_NAME);
		goto failed_class_create;
	}
	cdev_init(&(bbbgpiodev_ptr->cdev),&fops);
	bbbgpiodev_ptr->cdev.owner=THIS_MODULE;
	if (cdev_add(&(bbbgpiodev_ptr->cdev), bbbgpio_dev_no,1) != 0) {
		driver_err("%s: Cannot add device\n", DEVICE_NAME);
		goto failed_add_device;
	}
	bbbgpiodev_ptr->dev_ptr=device_create (bbbgpioclass_ptr, NULL, MKDEV(MAJOR(bbbgpio_dev_no), 0), NULL, DEVICE_PROCESS, 0);
	if (IS_ERR(bbbgpiodev_ptr->dev_ptr)){
		driver_err("%s: Cannot create device\n", DEVICE_NAME);
		goto failed_device_create;
	}
	driver_info ("%s: Registered device: (%d,%d)\n", DEVICE_NAME, MAJOR(bbbgpio_dev_no), MINOR(bbbgpio_dev_no));
	/* driver_info ("%s: Driver is loaded: ver. %d, built on %s, debug=%d\n", DEVICE_NAME, DRIVER_VERSION, __DATE__ " " __TIME__ , debug); */
	driver_info ("%s: Driver is loaded: ver. %d, debug=%d\n", DEVICE_NAME, DRIVER_VERSION, debug);
	return 0;

failed_device_create:
	{
		device_destroy(bbbgpioclass_ptr, MKDEV (MAJOR (bbbgpio_dev_no), 0));
		cdev_del(&(bbbgpiodev_ptr->cdev));
	}
failed_add_device:
	{
		class_destroy (bbbgpioclass_ptr);
		bbbgpioclass_ptr = NULL;
	}
failed_class_create:
	{
		unregister_chrdev_region (bbbgpio_dev_no, 1);
	}
failed_register:
	{
		kfree(bbbgpiodev_ptr);
		bbbgpiodev_ptr = NULL;
	}
failed_alloc:
	{
		return -EBUSY;
	}
}

static void 
__exit bbbgpio_exit(void){
		release_gpios();

        driver_info("%s: Unregistering...\n", DEVICE_NAME);
        if (bbbgpiodev_ptr != NULL) {
                device_destroy (bbbgpioclass_ptr, MKDEV(MAJOR(bbbgpio_dev_no), 0));
                cdev_del(&(bbbgpiodev_ptr->cdev));
                kfree(bbbgpiodev_ptr);
                bbbgpiodev_ptr = NULL;
        }
        unregister_chrdev_region(bbbgpio_dev_no, 1);
        if (bbbgpioclass_ptr != NULL) {
                class_destroy (bbbgpioclass_ptr);
                bbbgpioclass_ptr = NULL;
        }

        driver_info("%s: Driver unloaded\n", DEVICE_NAME);
}

module_param_named(debug, debug, int, 0644);
MODULE_PARM_DESC(debug, " Enable debug messages with debug=1");

MODULE_LICENSE ("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_SUPPORTED_DEVICE ("device")

module_init (bbbgpio_init);
module_exit (bbbgpio_exit);

