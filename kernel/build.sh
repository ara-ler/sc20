#/bin/bash

ARCH="arm"
INSTALL_MOD_PATH="./modules"
#CROSS_COMPILE="/home/sergeym/work/kernel/gcc/gcc/bin/arm-linux-gnueabihf-"
CROSS_COMPILE="/home/sergeym/work/gcc-7.5/bin/arm-linux-gnueabihf-"
#CROSS_COMPILE="/work/toolchains/gcc-7.5/bin/arm-linux-gnueabihf-"
OPTS="-j4"

# adjust local version
sed -i 's/\(CONFIG_LOCALVERSION=\).*$/\1"-'`date +%y%j`'"/' .config

if [ "x$1" != "x0" ] ; then
	echo "Continue with full build? Y | N"
	read answer
	if [ "$answer" != "y" ] && [ "$answer" != "Y" ] ; then
		echo "Exiting..."
		exit 1
	fi
	make clean
else
	echo BUILDING WITHOUT CLEANING
fi

make $OPTS ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE zImage-dtb modules
if [ $? -eq 0 ] ; then
	${CROSS_COMPILE}strip --strip-debug drivers/staging/prima/wlan.ko
	ls -l arch/arm/boot/zImage-dtb
	ls -l drivers/staging/prima/wlan.ko
	md5sum arch/arm/boot/zImage-dtb
	md5sum drivers/staging/prima/wlan.ko
fi

