#/bin/bash

ARCH="arm"
CROSS_COMPILE="/home/sergeym/work/kernel/gcc/gcc/bin/arm-linux-gnueabihf-"
OPTS="-j4"
export INSTALL_MOD_PATH=./modules

#make clean
make $OPTS ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE modules_install

VER=`sed -n 's/.*UTS_RELEASE.*"\(.*\)".*/\1/p' include/generated/utsrelease.h`
rm modules/lib/modules/$VER/build
rm modules/lib/modules/$VER/source

# copy the kernel
cp arch/arm/boot/zImage modules/vmlinuz-$VER

