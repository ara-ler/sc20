export ARCH=arm
#export TOOLCHAIN_PREFIX=/usr/bin/arm-linux-gnueabihf-
export TOOLCHAIN_PREFIX=/usr/bin/arm-none-eabi-
if [ "x$1" != "0" ] ; then
	make msm8909 clean
	./clean
fi
make msm8909
if [ -e build-msm8909/emmc_appsboot.mbn ] ; then
  md5sum build-msm8909/emmc_appsboot.mbn
fi

