#!/bin/bash

# Newhaven 7"
# convert png to raw
ffmpeg -vcodec png -i wallpaper-800x480.png -f rawvideo -pix_fmt bgra -y apergy-800x480.fb
# restore png from raw
ffmpeg -vcodec rawvideo -f rawvideo -pix_fmt bgra -s 800x480 -i apergy-rgb.fb -vcodec png -an wallpaper-800x480.png

# Resistive 10.4"
# convert png to raw
ffmpeg -vcodec png -i wallpaper-800x600.png -f rawvideo -pix_fmt rgb565 -y apergy-800x600.fb
# restore png from raw
ffmpeg -vcodec rawvideo -f rawvideo -pix_fmt rgb565 -s 800x600 -i apergy-bgr.fb -vcodec png -an wallpaper-800x600.png

# Hantronix 10.1"
# convert png to raw
ffmpeg -vcodec png -i wallpaper-1280x800.png -f rawvideo -pix_fmt bgra -y apergy-1280x800.fb
# restore png from raw
ffmpeg -vcodec rawvideo -f rawvideo -pix_fmt bgra -s 1280x800 -i apergy-rgb.fb -vcodec png -an wallpaper-1280x800.png


