#!/bin/sh

SRC=mcm_atcop_svc_hdlr.c
CFLAGS="-I../inc/ql-mcm-api -I../inc/qmi-framework -I../ipc"
LDFLAGS="-lpthread -lrt -lmcm -lqcmap_client -lql_mcm_api -lqmi_csi -lmcmipc -lmcm_ssr_util -ldiag"

arm-linux-gnueabihf-gcc -fpic -pthread -o at-server $CFLAGS $LDFLAGS --sysroot=/home/sergeym/work/images/evalboard $SRC
