// Copyright 2021 (c) ChampionX
// All rights reserved

#include <stdio.h>
#include <stdlib.h>
#include <ql-mcm-api/ql_in.h>
#include <getopt.h>

#define IF_UP_CMD	"ip -br a show dev rmnet_data0 | grep -q '[0-9\\./]\\{9,18\\}'"
#define AIRPLANE_ON			1
#define AIRPLANE_OFF		2

#define EXIT_CARRIER	0
#define EXIT_FAILED		1
#define EXIT_AIRPLANE	2
#define EXIT_NOAPNSET	3

static data_client_handle_type  h_data = 0;
static dm_client_handle_type    h_dm = 0;

#define ATC_REQ_CMD_MAX_LEN     513
#define ATC_RESP_CMD_MAX_LEN    4097
static char atc_cmd_req[ATC_REQ_CMD_MAX_LEN]    = {0};
static char atc_cmd_resp[ATC_RESP_CMD_MAX_LEN]  = {0};
static atc_client_handle_type    h_atc     = 0;


char *addr2str(QL_MCM_DATA_CALL_ADDR_T *addr, char *buf, int len, int *pIsIpv6)
{
    char *ptr;
    int i;
    int isIpv6 = 0;

    if(addr->valid_addr==0) {
        return NULL;
    }
    
    if(addr->addr.ss_family == AF_INET6) {
        isIpv6 = 1;
    }

    if(pIsIpv6) {
        pIsIpv6[0] = isIpv6;
    }
    if(isIpv6==0) {
        snprintf(buf, len, "%d.%d.%d.%d", addr->addr.__ss_padding[0], 
                addr->addr.__ss_padding[1],addr->addr.__ss_padding[2],addr->addr.__ss_padding[3]);
    }
    else {
        inet_ntop(AF_INET6, &(addr->addr), buf, sizeof(addr->addr));
    }

    return buf;
}

static void ql_mcm_data_nfy (data_client_handle_type h_data, E_QL_DATA_NET_EVENT_MSG_ID_T e_msg_id, QL_MCM_DATA_CALL_INDICATION_INFO_T *data, void *contextPtr)
{
    int is_ipv6 = 0;
    char *event[] = {   "unknown", 
                        "NET_UP", 
                        "NET_DOWN", 
                        "NEW_ADDR", 
                        "DEL_ADDR", 
                        "SERVICE_STATUS", 
                        "BEARER_TECH_STATUS",
                        "DORMANCY_STATUS"};
    char *call_status[] = {   "INVALID", 
                        "INVALID", 
                        "CONNECTING",
                        "CONNECTED",
                        "DISCONNECTING",
                        "DISCONNECTED"};
    printf("### %s got %s event!\n", __func__, event[e_msg_id - 0x5000]);

    switch(e_msg_id)
    {
    case E_QL_DATA_NET_UP_EVENT:
        printf("Event: Network UP\n");
        break;
    case E_QL_DATA_NET_DOWN_EVENT:
        printf("Event: Network DOWN\n");
        break;
    case E_QL_DATA_NET_NEW_ADDR_EVENT:
        printf("Event: Got new address\n");
        break;
    case E_QL_DATA_NET_DEL_ADDR_EVENT:
        printf("Event: Address deleted\n");
        break;
    case E_QL_DATA_NET_SERVICE_STATUS_EVENT:
        printf("Event: Service Status\n");
        break;
    case E_QL_DATA_NET_BEARER_TECH_STATUS_EVENT:
        printf("Event: Tech Status\n");
        break;
    case E_QL_DATA_NET_DORMANCY_STATUS_EVENT:
        printf("Event: Dormancy Status\n");
        break;
    }
    
    if(data->call_id_valid) 
    {
        printf("call_id : %d\n", data->call_id);
    }
    if(data->call_status_valid) 
    {
        printf("call_status : %s\n", call_status[data->call_status]);
    }
    if(data->call_tech_valid) 
    {
        printf("call_tech : %d\n", data->call_tech);
    }
    if(data->reg_status_valid) 
    {
        printf("reg_status : %d\n", data->reg_status);
    }
    if(data->dorm_status_valid)
    {
        printf("dorm_status : %d\n", data->dorm_status);
    }
    if(data->addr_count_valid) 
    {
        printf("addr_count  : %d\n", data->addr_count);
    }
    if(data->addr_info_valid) 
    {
        int i;
        QL_MCM_DATA_CALL_ADDR_INFO_T *addr;
        char *ptr;
        char tmpBuf[246];
        struct in_addr ia;
        printf("addr_info_len : %d\n", data->addr_info_len);
        for(i=0; i<data->addr_info_len; i++) 
        {
            addr = &data->addr_info[i];
            ptr = addr2str(&addr->iface_addr_s, tmpBuf, sizeof(tmpBuf), &is_ipv6);
            if(ptr) 
            {
                printf("IP Address: %s/%d \n", ptr, addr->iface_mask);
            }

            ptr = addr2str(&addr->gtwy_addr_s, tmpBuf, sizeof(tmpBuf), &is_ipv6);
            if(ptr) 
            {
                printf("Gateway: %s/%d \n", ptr, addr->gtwy_mask);
            }

            ptr = addr2str(&addr->dnsp_addr_s, tmpBuf, sizeof(tmpBuf), &is_ipv6);
            if(ptr) 
            {
                printf("Primary DNS: %s\n", ptr);
            }

            ptr = addr2str(&addr->dnss_addr_s, tmpBuf, sizeof(tmpBuf), &is_ipv6);
            if(ptr) 
            {
                printf("Secondary DNS: %s\n", ptr);
            }
        }
    }
    if(data->vce_reason_valid) 
    {
        printf("data call end reason : %d\n", data->vce_reason);
    }
}

static int data_call(char *apn, char *username, char *password)
{
    int	profile_id = 1;
    E_QL_ERROR_CODE_T ret;
    QL_MCM_DATA_CALL_RESULT_INFO_T  t_call_result = {0};
    QL_MCM_DATA_CALL_CONFIG_INFO_T  t_cfg = {0};

    memset(&t_cfg, 0, sizeof(t_cfg));
    memset(&t_call_result, 0, sizeof(t_call_result));

	ret = QL_MCM_DATA_Client_Init(&h_data);
	sleep (1);
	printf("\n\nQL_MCM_DATA_Client_Init():  ret = %d, h_data = 0x%X\n", ret, h_data);
	ret = QL_MCM_DATA_AddRxIndMsgHandler((QL_DATA_RxIndMsgHandlerFunc_t)ql_mcm_data_nfy, (void*)h_data);

	ret = QL_MCM_DATA_SetProfile(&t_cfg, profile_id, TRUE);
	printf("\n\nQL_MCM_DATA_SetProfile():  ret = %d\n", ret);

	ret = QL_MCM_DATA_SetAPN(&t_cfg, apn, TRUE);
	printf("\n\nQL_MCM_DATA_SetAPN():  APN=%s ret = %d\n", apn, ret);

	if (NULL != username) {
		ret = QL_MCM_DATA_SetUsername(&t_cfg, username, TRUE);
		printf("QL_MCM_DATA_SetUsername username = %s ret = %d\n", username, ret);
	}

	if (NULL != password) {
		ret = QL_MCM_DATA_SetPassword(&t_cfg, password, TRUE);
		printf("QL_MCM_DATA_SetPassword password = %c... ret = %d\n", password[0], ret);
	}

	ret = QL_MCM_DATA_StartDataCall(h_data, &t_cfg, &t_call_result);
	printf("\n\nQL_MCM_DATA_StartDataCall():  ret = %d, callid = %d \n", ret, t_call_result.call_id);

	return t_call_result.call_id;
}

static int airplane_set(int mode)
{
	E_QL_MCM_DM_AIRPLANE_MODE_TYPE_T airplane_mode;
    E_QL_ERROR_CODE_T ret;

	ret = QL_MCM_DM_Client_Init(&h_dm);
    printf("\n\nQL_MCM_DM_Client_Init():  ret = %d, h_dm = 0x%X\n", ret, h_dm);

	ret = QL_MCM_DM_GetAirplaneMode(h_dm, &airplane_mode);
    printf("\n\nQL_MCM_DM_GetAirplaneMode():  ret = %d, airplane_mode = %d (1 = ON, 2 = OFF)\n", ret, airplane_mode);
	if (ret == 0 && airplane_mode == mode)
		return airplane_mode;

	ret = QL_MCM_DM_SetAirplaneMode(h_dm, 2);
    printf("\n\nQL_MCM_DM_SetAirplaneMode():  ret = %d\n", ret);
	ret = QL_MCM_DM_GetAirplaneMode(h_dm, &airplane_mode);
    printf("\n\nQL_MCM_DM_GetAirplaneMode():  ret = %d, airplane_mode = %d (1 = ON, 2 = OFF)\n", ret, airplane_mode);
	if (ret == 0 && airplane_mode == mode) {
		sleep (16);
		return airplane_mode;
	}
	else
		return -1;
}

static void usage(char *argv0)

{
	fprintf (stderr, "usage:  %s -a -u -p -h\n", argv0);
	fprintf (stderr, "  -a <APN>\n");
	fprintf (stderr, "  -u <username>\n");
	fprintf (stderr, "  -p <password>\n");
	fprintf (stderr, "  -h - display this help\n");
	fprintf (stderr, "APN is mandatory, username and password are optional\n");
	exit(-1);
}

int main(int argc, char** argv)
{
    char *apn = NULL;
	char *username = NULL;
	char *password = NULL;
	int option;
    int ret;
	bool at_mode = true;

	while ((option = getopt (argc, argv, "?ha:u:p:A:")) != -1) {
		switch (option) {
		case 'a':
			apn = optarg;   // optarg is an extern from the getopt() facility.
			break;
		case 'u':
			username = optarg;
			break;
		case 'p':
			password = optarg;
			break;
		case 'A':
			if (!strncmp("api", optarg, 3))
				at_mode = false;
			break;
		default:
			usage(argv[0]);
			break;
		}
	}

	if (at_mode) {
		ret = QL_ATC_Client_Init(&h_atc);
		printf("QL_ATC_Client_Init ret=%d with h_atc=0x%x\n", ret, h_atc);

		//memset(atc_cmd_req,  0, sizeof(atc_cmd_req));
		memset(atc_cmd_resp, 0, sizeof(atc_cmd_resp));
		
		sprintf (atc_cmd_req, "AT+CFUN=1");
		ret = QL_ATC_Send_Cmd(h_atc, atc_cmd_req, atc_cmd_resp, ATC_RESP_CMD_MAX_LEN);
		printf("QL_ATC_Send_Cmd(): %s ret = %d, responce = \n%s\n", atc_cmd_req, ret, atc_cmd_resp);

 		sprintf (atc_cmd_req, "AT+CGDCONT=1,\"IPV4V6\",\"%s\"", apn);
		ret = QL_ATC_Send_Cmd(h_atc, atc_cmd_req, atc_cmd_resp, ATC_RESP_CMD_MAX_LEN);
		printf("QL_ATC_Send_Cmd(): %s ret = %d, responce = \n%s\n", atc_cmd_req, ret, atc_cmd_resp);
        
		sprintf (atc_cmd_req, "AT+QMBNCFG=\"AutoSel\"");
		ret = QL_ATC_Send_Cmd(h_atc, atc_cmd_req, atc_cmd_resp, ATC_RESP_CMD_MAX_LEN);
		printf("QL_ATC_Send_Cmd(): %s ret = %d, responce = \n%s\n", atc_cmd_req, ret, atc_cmd_resp);

		ret = QL_ATC_Client_Deinit(h_atc);
		printf("QL_ATC_Client_Deinit ret=%d\n", ret);
 
	}
	else {
		if (AIRPLANE_OFF != airplane_set (AIRPLANE_OFF)) {
			printf("\nCannot disable airplane mode, exiting...\n");
			return EXIT_AIRPLANE;
		}
	}

	if (NULL != apn) {
		ret = data_call (apn, username, password);
		if (ret <= 0) {
			printf("\nData call failed, exiting...\n");
			return EXIT_FAILED;
		}
	}
	else {
		printf("\nNO APN set, exiting...\n");
		return EXIT_NOAPNSET;
	}

	printf("\nSuccess. Press Ctrl-C to terminate data call.\n");
	ret = 0;
	while (ret == 0) {
		sleep (5);
		ret = system (IF_UP_CMD);
	}
	printf("\nData interface is unconfigured. Restart me, exiting now...\n");
	return EXIT_CARRIER;
}


