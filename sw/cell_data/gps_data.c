// Copyright 2021 (c) ChampionX
// All rights reserved

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ql_in.h>

#define OUTPUT_FILE "/tmp/GPScoordinates"
#define ACQ_DEF_INTERVAL	60

static int h_loc = 0;

static void ql_msg(loc_client_handle_type h_loc, E_QL_LOC_NFY_MSG_ID_T e_msg_id, void *pv_data, void *context_ptr)
{
	// DEBUG:
    // printf("e_msg_id=%d\n", e_msg_id); 
    switch(e_msg_id)
    {
        case E_QL_LOC_NFY_MSG_ID_STATUS_INFO:
            break;
        case E_QL_LOC_NFY_MSG_ID_LOCATION_INFO:
        {
            QL_LOC_LOCATION_INFO_T *pt_location = (QL_LOC_LOCATION_INFO_T *)pv_data;
			// DEBUG:
            // printf("flag=0x%X, Latitude = %f, Longitude=%f, accuracy = %f ****\n", pt_location->flags, pt_location->latitude, pt_location->longitude, pt_location->accuracy);
            break;
        }
        case E_QL_LOC_NFY_MSG_ID_SV_INFO:
            break;
        case E_QL_LOC_NFY_MSG_ID_NMEA_INFO:
        {
            QL_LOC_NMEA_INFO_T *pt_nmea = (QL_LOC_NMEA_INFO_T *)pv_data;
			// DEBUG:
            // printf("NMEA info: timestamp=%lld, length=%d, nmea=%s\n", pt_nmea->timestamp, pt_nmea->length, pt_nmea->nmea); 
            break;
        }
        case E_QL_LOC_NFY_MSG_ID_CAPABILITIES_INFO:
            break;
        case E_QL_LOC_NFY_MSG_ID_AGPS_STATUS:
            break;
        case E_QL_LOC_NFY_MSG_ID_NI_NOTIFICATION:
            break;
        case E_QL_LOC_NFY_MSG_ID_XTRA_REPORT_SERVER:
            break;
    }
}

int main(int argc, char** argv)
{
	QL_LOC_INJECT_TIME_INTO_T	t_info = {0};
	QL_LOC_LOCATION_INFO_T		t_loc_info = {0};
	int			cmdIdx = 0;
	int			ret = E_QL_OK;
	time_t		time_cur;
	time_t		time_prev;
	int			timeout_sec = 600;
	int			acq_interval = ACQ_DEF_INTERVAL;

	QL_LOC_Client_Init(&h_loc);
	QL_LOC_AddRxIndMsgHandler(ql_msg, (void*)h_loc);
	QL_LOC_Set_Indications(h_loc, 0x1FF);

	if (argc > 1) {
		acq_interval = atoi(argv[1]);
		if (acq_interval <= 0)
			acq_interval = ACQ_DEF_INTERVAL;
	}

	while (1) {
		time(&time_cur);
		time_prev = time_cur;
		ret = QL_LOC_Get_Current_Location(h_loc, &t_loc_info, timeout_sec);
		printf("QL_LOC_Get_Current_Location ret(): ret=%d\n", ret);
		if(ret < 0) {
			if (ret == -2) {
				printf("QL_LOC_Get_Current_Location():  timeout, try again!\n");
			}
			else {
				printf("QL_LOC_Get_Current_Location():  failed!\n");
			}
		}
		else {
			FILE *fout = 0;
			time(&time_cur);
			fout = fopen(OUTPUT_FILE, "w");
			if (fout > 0) {
				fprintf(fout, "%lf\n%lf\n%lf\n%f\n%ld\n%ld\n", t_loc_info.latitude, t_loc_info.longitude, t_loc_info.altitude, t_loc_info.accuracy, (long)(time_cur - time_prev), time_cur);
				fclose(fout);
				sleep (1);
				printf ("\n\nLatitude, Longitude, Altitude, Accuracy, Acquisition time, Timestamp:\n");
				system ("/bin/cat " OUTPUT_FILE);
				printf ("========== end of " OUTPUT_FILE " ==========\n");
			}
			else {
				printf ("Cannot write " OUTPUT_FILE " file\n");
				printf(">>>>>> Latitude = %lf, Longitude = %lf, Altitude = %lf, Accuracy = %f Acq. time = %ld s\n", t_loc_info.latitude, t_loc_info.longitude, t_loc_info.altitude, t_loc_info.accuracy, (long)(time_cur - time_prev));
			}
			time_prev = time_cur;
		}
		sleep (acq_interval);
	}
	//printf("QL_LOC_Client_Deinit:%d\n",QL_LOC_Client_Deinit(h_loc));
    return 0;
}


