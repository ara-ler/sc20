/* Copyright 2021 (c) ChampionX */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <signal.h>
#include <linux/fb.h>
#include <sys/mman.h>

//#define FBIOGET_VSCREENINFO	0x4600
//#define FBIOPUT_VSCREENINFO	0x4601
//#define FBIOBLANK		0x4611		/* arg: 0 or vesa level + 1 */

//struct fb_var_screeninfo {
//   __u32 xres;			/* visible resolution		*/
//   __u32 yres;
//   __u32 xres_virtual;		/* virtual resolution		*/
//   __u32 yres_virtual;
//   __u32 xoffset;			/* offset from virtual to visible */
//   __u32 yoffset;			/* resolution			*/
 
//   __u32 bits_per_pixel;		/* guess what			*/
//   __u32 grayscale;		/* 0 = color, 1 = grayscale,	*/
//   				/* >1 = FOURCC			*/
//   struct fb_bitfield red;		/* bitfield in fb mem if true color, */
//   struct fb_bitfield green;	/* else only length is significant */
//   struct fb_bitfield blue;
//   struct fb_bitfield transp;	/* transparency			*/	
 
//   __u32 nonstd;			/* != 0 Non standard pixel format */
 
//   __u32 activate;			/* see FB_ACTIVATE_*		*/
 
//   __u32 height;			/* height of picture in mm    */
//   __u32 width;			/* width of picture in mm     */
 
//   __u32 accel_flags;		/* (OBSOLETE) see fb_info.flags */
 
//   /* Timing: All values in pixclocks, except pixclock (of course) */
//   __u32 pixclock;			/* pixel clock in ps (pico seconds) */
//   __u32 left_margin;		/* time from sync to picture	*/
//   __u32 right_margin;		/* time from picture to sync	*/
//   __u32 upper_margin;		/* time from sync to picture	*/
//   __u32 lower_margin;
//   __u32 hsync_len;		/* length of horizontal sync	*/
//   __u32 vsync_len;		/* length of vertical sync	*/
//   __u32 sync;			/* see FB_SYNC_*		*/
//   __u32 vmode;			/* see FB_VMODE_*		*/
//   __u32 rotate;			/* angle we rotate counter clockwise */
//   __u32 colorspace;		/* colorspace for FOURCC-based modes */
//   __u32 reserved[4];		/* Reserved for future compatibility */
//};

// #define SCREEN_SIZE		1152000		/* 800*480*3 (24bpp) */
#define SCREEN_SIZE		3072000		/* 1280*800*3 (24bpp) */

static bool keepgoing = true;

void sig_handler(int s)
{
	printf ("Signal caught, exiting\n");
	keepgoing = false;
}

int main()
{

	int fd;
	struct fb_var_screeninfo scr_info;
	void *mp;

	signal(SIGTERM, sig_handler);
	signal(SIGINT, sig_handler);

	memset (&scr_info, 0, sizeof(scr_info));

	fd = open("/dev/fb0", O_RDWR);

	/* the original call from Quectel's ql-fb-service */
	//mmap(NULL, 972112, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0);
	//mp = mmap(NULL, SCREEN_SIZE, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, fd, 0);
	mp = mmap (NULL, SCREEN_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	ioctl(fd, FBIOGET_VSCREENINFO, &scr_info);  
	printf ("Before:\nxres=%u, yres=%u, pixclock=%u, hsync_len=%u, vsync_len=%u, activate=%u \n",
			scr_info.xres, scr_info.yres, scr_info.pixclock, scr_info.hsync_len, scr_info.vsync_len, scr_info.activate);

	printf ("Before:\nxleft=%u, right=%u, upper=%u, lower=%u, vsync_len=%u, sync=%u, vmode=%u \n", \
    scr_info.left_margin, \
    scr_info.right_margin, \
    scr_info.upper_margin, \
    scr_info.lower_margin, \
    scr_info.sync, \
    scr_info.vmode);

	/*
	ioctl(fd, FBIOBLANK, 2); 
	sleep (1);
	ioctl(fd, FBIOBLANK, 0);
	sleep (1);
	scr_info.xres = 800;
	scr_info.yres = 480;
	scr_info.hsync_len = 80;
	scr_info.vsync_len = 30;
	*/

	int l,t,w,h;
	l=0;
    t=0;
    w=1280;
    h=800;
	scr_info.reserved[0] = 0x54445055; // "UPDT";
    scr_info.reserved[1] = (uint16_t)l | ((uint32_t)t << 16);
    scr_info.reserved[2] = (uint16_t)(l+w) | ((uint32_t)(t+h) << 16);

	scr_info.hsync_len = 48;
	scr_info.vsync_len = 3;
    scr_info.left_margin = 60;
    scr_info.right_margin = 60;
    scr_info.upper_margin = 10;
    scr_info.lower_margin = 10;
    //scr_info.vmode = 0;

	//scr_info.pixclock = 25031; // for some reason we have to change the pixclock, but the actual clock doesn't change
	//scr_info.pixclock = 40960;
	scr_info.pixclock = 71100;
	ioctl(fd, FBIOPUT_VSCREENINFO, &scr_info);

	ioctl(fd, FBIOGET_VSCREENINFO, &scr_info);  
	printf ("After:\nxres=%u, yres=%u, pixclock=%u, hsync_len=%u, vsync_len=%u, activate=%u \n",
			scr_info.xres, scr_info.yres, scr_info.pixclock, scr_info.hsync_len, scr_info.vsync_len, scr_info.activate);
	printf ("After:\nxleft=%u, right=%u, upper=%u, lower=%u, sync=%u, vmode=%u\n", \
    scr_info.left_margin, \
    scr_info.right_margin, \
    scr_info.upper_margin, \
    scr_info.lower_margin, \
    scr_info.sync, \
    scr_info.vmode);

	while (keepgoing) {
		pause ();
	}

	ioctl(fd, FBIOBLANK, 4); // powerdown
	munmap (mp, SCREEN_SIZE);
	close (fd);
	return 0;
}

