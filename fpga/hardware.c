/*********************************************************************
* (c) 2020 ChampionX
***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "bbbgpio_ioctl.h"
#include "hardware.h"

unsigned int a_uiCheckFailedRow = 0;
unsigned int a_uiRowCount		= 0;

static const char *device = SPI_DEV;
static uint8_t mode = 0;
static uint8_t bits = 8;
static uint32_t speed = 19200000/4;	// max: 19200000 min: 1200000 ?
static uint16_t delay = 0;
static int fd;
static int bbbFd = -1;

int gpioInit()
{
	if (bbbFd < 0) bbbFd = open (BBB_DEV, O_RDWR);

	if (bbbFd < 0)
		printf ("gpioInit(): fd=%d error: %s\n", bbbFd, strerror (errno));
	else
		if (debug) printf ("GPIO Interface is initialized\n");
	return bbbFd;
}

void gpioFinish()
{
	if (bbbFd > 0) {
		close (bbbFd);
		if (debug) printf ("GPIO Interface is closed\n");
	}
	else
		printf ("gpioFinish(): cannot close fd=%d\n", bbbFd);
}

int gpioControl(int gpio_num, int dir, int val)
{
	struct bbb_ioctl_struct ioctl_struct;
	int ret = -255;

	if (bbbFd < 0) {
		printf ("gpioControl(): fd=%d error: %s\n", bbbFd, strerror (errno));
		return -1;
	}

	ioctl_struct.pin_or_port_num = gpio_num;
	ioctl_struct.value = dir;
	if (ioctl (bbbFd, PIN_SETDIR, &ioctl_struct) != 0) {
		printf ("PIN_SETDIR was not set to %d\n", dir);
		return -2;
	}
	if (dir == OUT_DIR) {
		ioctl_struct.value = val;
		if (-1 == write (bbbFd, &ioctl_struct, sizeof (struct bbb_ioctl_struct))) {
			printf ("Cannot write gpio=%d, value=%d\n", gpio_num, val);
			return -3;
		}
		// success
		return 255;
	}
	else
	{
		if (-1 == read (bbbFd, &ioctl_struct, sizeof (struct bbb_ioctl_struct))) {
			printf ("Cannot read gpio=%d\n", gpio_num);
			return -4;
		}
		// success
		return ioctl_struct.value;
	}
	return ret;
}

void fpgaReset()
{
	if (debug) printf ("Resetting FPGA: pins: cs=%d reset=%d erase=%d done=%d\n", SPI_CS, FPGA_RES, FPGA_ERASE);

	gpioControl(FPGA_RES, OUT_DIR, LOW);
	wait(10);
	gpioControl(SPI_CS, OUT_DIR, LOW);
	wait(10);
	gpioControl(FPGA_RES, OUT_DIR, HIGH);

	//gpioControl(FPGA_ERASE, OUT_DIR, LOW);
	//wait(10);
	//gpioControl(SPI_CS, OUT_DIR, LOW);
	//wait(10);
	//gpioControl(FPGA_ERASE, OUT_DIR, HIGH);
}


int SPIinit()
{
	int ret;

	if (debug) printf ("Initializing SPI device %s\n", device);
	fd = open(device, O_RDWR);
	if (fd < 0)
		printf("can't open device\n");
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		printf("can't set spi mode\n");
	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		printf("can't get spi mode\n");
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't set bits per word\n");
	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't get bits per word\n");
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't set max speed hz\n");
	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't get max speed hz\n");

	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz\n", speed);
	
	return 1;
}

int SPIend()
{
	if (debug) printf ("Closing SPI device\n");
	close (fd);
	return 1;
}

int wait (int delay_in_ms)
{
	if (debug) printf ("Delaying for %d ms\n", delay_in_ms);
	usleep (delay_in_ms * 1000);
	return 1;
}

int txBytes(unsigned char *trBuffer, int trCount)
{
	int ret;
	unsigned char rxBuffer[1024];

	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)trBuffer,
		.rx_buf = (unsigned long)rxBuffer,
		.len = trCount,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};

	if (trCount > 1024)
		return 0;

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	return 1;
}

int TRANS_receiveBytes(unsigned char *rcBuffer, int rcCount)
{
	return 1;
}

int txStart()
{
	if (debug) printf ("Start of transmission, CS=%d set LOW\n", SPI_CS);
	gpioControl(SPI_CS, OUT_DIR, LOW);
	return 1;
}

int txEnd()
{
	if (debug) printf ("End of transmission, CS=%d set HIGH\n", SPI_CS);
	gpioControl(SPI_CS, OUT_DIR, HIGH);
	return 1;
}

int eraseToggle(unsigned char value)
{
	if (debug) printf ("Reset pin = %d toggle = %d\n", FPGA_ERASE, value);
	if (value == 0)
		gpioControl(FPGA_ERASE, OUT_DIR, LOW);
	else
		gpioControl(FPGA_ERASE, OUT_DIR, HIGH);
	return 1;
}

int csToggle()
{
	if (debug) printf ("Toggle CS: CS pin = %d \n", SPI_CS);
	gpioControl(SPI_CS, OUT_DIR, HIGH);
	gpioControl(SPI_CS, OUT_DIR, LOW);
	gpioControl(SPI_CS, OUT_DIR, HIGH);

	return 1;
}

int clkRun(int clks)
{
	unsigned char *txbuf;
	int bytes = clks / 8;

	if (debug) printf ("Running %d clocks\n", clks);

	txbuf = malloc(bytes);
	memset (txbuf, 0xFF, bytes);
	txBytes (txbuf, bytes);
	free (txbuf);
	return 1;
}

