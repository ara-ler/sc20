#ifndef _HARDWARE_H_
#define _HARDWARE_H_

// FPGA:
// BBB = ICE40 (ICE40HX8K-CT256)
// CROSS = Crosslink (LIF-MD6000-6JMG80I)
#define BBB_DEV		"/dev/bbbgpio0"
#define SPI_DEV		"/dev/spidev0.0"
#define IN_DIR      0
#define OUT_DIR     1
#define LOW			0
#define HIGH		1

#define SPI_CLK		11
#define SPI_MISO	9
#define SPI_MOSI	8

#define SPI_CS_BBB			0
#define FPGA_RES_BBB		16	// there is only one "reset" pin for ICE40
#define FPGA_ERASE_BBB		16	// so, ERASE and RESET are the same

#define SPI_CS_CROSS		36
#define FPGA_RES_CROSS		24
#define FPGA_ERASE_CROSS	65

#define FPGA_BBB	0 
#define FPGA_CROSS	1 

#define SPI_CS		(fpgaIndex==FPGA_BBB?SPI_CS_BBB:SPI_CS_CROSS)
#define FPGA_RES	(fpgaIndex==FPGA_BBB?FPGA_RES_BBB:FPGA_RES_CROSS)
#define FPGA_ERASE	(fpgaIndex==FPGA_BBB?FPGA_ERASE_BBB:FPGA_ERASE_CROSS)

#define	LCD_TE		24

extern int fpgaIndex;
extern int debug;

int SPIinit();
int SPIend();
int wait(int ms);
void fpgaReset(void);
int txStart(void);
int txEnd();
int csToggle(void);
int eraseToggle(unsigned char value);
int clkRun(int clk);
int txBytes(unsigned char *trBuffer, int trCount);
int gpioInit(void);
void gpioFinish(void);
int gpioControl(int gpio_num, int dir, int val);

#endif

