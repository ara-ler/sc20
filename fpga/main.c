/**************************************************************
* (c) 2020 ChampionX
***************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>
#include "hardware.h"
#include "bitstream.h"
#include "bbb.h"

#ifdef LCD_TEST
#include "lcd-test.h"
#else
#include "lcd.h"
#endif

int debug = 0;
int fpgaIndex = 0;

void printUsage(){
	printf( "Usage: fpga <index> [debug]\n" );
	printf( "       where index is 0 for FPGA_BBB and 1 for FPGA_CROSS\n" );
}

void bitstreamLCD()
{
	int i, j;

	fpgaReset();
	SPIinit();
	gpioControl(LCD_TE, OUT_DIR, HIGH);

	eraseToggle(1);
	wait(1);
	eraseToggle(0);
	wait(10);

	txStart();
	printf("Transmitting %u bytes\n", sizeof(streamLCD1));
	txBytes(streamLCD1, sizeof(streamLCD1));
	txEnd();

	eraseToggle(1);
	wait(1);

	printf("Transmitting %u bytes\n", sizeof(streamLCD2));
	txStart();
	txBytes(streamLCD2, sizeof(streamLCD2));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD3));
	txStart();
	txBytes(streamLCD3, sizeof(streamLCD3));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD4));
	txStart();
	txBytes(streamLCD4, sizeof(streamLCD4));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD5));
	txStart();
	txBytes(streamLCD5, sizeof(streamLCD5));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD6));
	txStart();
	txBytes(streamLCD6, sizeof(streamLCD6));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD7));
	txStart();
	txBytes(streamLCD7, sizeof(streamLCD7));
	txEnd();

	int offset = 259;
	int size = lcdSize - offset;
	int chunk=26;
	unsigned char *frame;

	printf("Transmitting %u bytes\n", size);
	for (i=0; i<size/chunk; i++) {
		txStart();
		txBytes(frameHead, sizeof(frameHead));
		frame = &lcdData[offset+i*chunk];
		txBytes(frame, chunk);
		if (debug) {
			printf ("Frame: ");
			for (j=0; j<chunk ; j++)
				printf ("%02X ", (unsigned char)frame[i]);
			printf("\n");
		}
		txEnd();
	}

	wait(1);

	printf("Transmitting %u bytes\n", sizeof(streamLCD8));
	txStart();
	txBytes(streamLCD8, sizeof(streamLCD8));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD9));
	txStart();
	txBytes(streamLCD9, sizeof(streamLCD9));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD6a));
	txStart();
	txBytes(streamLCD6a, sizeof(streamLCD6a));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD6b));
	txStart();
	txBytes(streamLCD6b, sizeof(streamLCD6b));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD6c));
	txStart();
	txBytes(streamLCD6c, sizeof(streamLCD6c));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD6d));
	txStart();
	txBytes(streamLCD6d, sizeof(streamLCD6d));
	txEnd();

	printf("Transmitting %u bytes\n", sizeof(streamLCD6e));
	txStart();
	txBytes(streamLCD6e, sizeof(streamLCD6e));
	txEnd();

	wait(1);
	SPIend();

	//gpioControl(LCD_TE, OUT_DIR, LOW);
	//wait(2);
	//gpioControl(LCD_TE, OUT_DIR, HIGH);
}

void bitstreamBBB()
{
	int i;

	fpgaReset();
	SPIinit();

	txStart();
	eraseToggle(1);
	eraseToggle(0);
	wait(2);

	eraseToggle(0);
	eraseToggle(1);
	wait(2);
	txEnd();

	wait(10);

	int offset = 259; // the .c file corresponds the .bin file starting from 259 (0-based) 0xFF 0x00 ...
	int size = bbbSize - offset;
	int chunk = 1000;

	printf("Transmitting %u bytes\n", size);
	txStart();
	for (i=0; i<size/chunk; i++) {
		if (debug) printf("Chunk %d of %d bytes\n", i, chunk);
		txBytes(&bbbData[offset+i*chunk], chunk);
	}
	if (debug) printf("Chunk %d of %d bytes\n", i, size % chunk);
	txBytes(&bbbData[offset+i*chunk], size % chunk);
	txEnd();

	clkRun(chunk);

	SPIend();
}

int main(int argc, char * argv[])
{
	printf ("FPGA programmer, (c) 2020 ChampionX\n");

	if (argc >= 2) {
		fpgaIndex = atoi (argv[1]);

		if (argc > 2 && !strncmp("debug", argv[2], 5)) {
			debug = 1;
		}
		gpioInit();

		if (fpgaIndex !=0) {
			printf("Programming Crosslink FPGA (LCD)...\n");
			bitstreamLCD();
		}
		else {
			printf("Programming BBB Interface FPGA...\n");
			bitstreamBBB();
		}

		gpioFinish();
		return 0;
	}
	else {
		printUsage();
		return 1;
	}
	return 0;
} 

