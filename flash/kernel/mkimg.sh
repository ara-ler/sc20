#!/bin/sh

#########
# Simple build of kernel and wlan module
#
#########

BASE=../..

KERNEL=kernel.img
KERNEL_RAW=zImage-dtb
MODULE=wlan.ko
WLANIMG=wlan.img
KERN_PATH=${BASE}/kernel
FLASH_PATH=${BASE}/flash
ZIMAGE="${KERN_PATH}/arch/arm/boot/$KERNEL_RAW"
WLAN="${KERN_PATH}/drivers/staging/prima/$MODULE"
MD5CMD="md5sum"
MD5SUM="md5.sum"
CMD_LINE=${FLASH_PATH}/kernel/cmdline.txt

MKFSEXT4=/sbin/mkfs.ext4
MKIMG=${FLASH_PATH}/tools/mkbootimg/mkbootimg

rm $KERNEL
rm $MODULE
rm $WLANIMG
rm $MD5SUM

$MKIMG --kernel ${ZIMAGE} --cmdline "`cat ${CMD_LINE}`" --base "81c00000" --pagesize 4096 --kernel_offset "00008000" -o $KERNEL

ls -l $KERNEL
ls -l $WLAN
echo -n 1\ new\ >> $MD5SUM
${MD5CMD}  $KERNEL >> $MD5SUM
echo -n 3\ new\ >> $MD5SUM
${MD5CMD} $WLAN >> $MD5SUM
${MD5CMD} $ZIMAGE
sort $MD5SUM

echo Copying $MODULE to image file...
sudo umount /mnt/tmp >/dev/null 2>&1

if [ ! -e $WLANIMG ] ; then
    dd if=/dev/zero of=$WLANIMG bs=1M count=20 
    ${MKFSEXT4} -m 0.01 wlan.img
fi

if [ "x`uname`" = "xFreeBSD" ] ; then
    sudo lklfuse -o type=ext4 $WLANIMG /mnt/tmp
    BS=1m
else
    if [ "x`uname`" = "xLinux" ] ; then
	sudo mount $WLANIMG /mnt/tmp
	BS=1M
    else
    	echo Don\'t know how to mount $WLANIMG
    fi
fi

mount | grep -q '/mnt/tmp'
if [ "$?" -eq 0 ] ; then
    xz -kf $WLAN
    sudo cp ${WLAN}.xz /mnt/tmp/
    sudo umount /mnt/tmp
    dd if=wlan.img of=$KERNEL seek=10 bs=$BS conv=notrunc
else
    echo Cannot copy $MODULE.xz
fi

