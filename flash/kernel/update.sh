#!/bin/sh

KERNEL=kernel.img
KERNEL_RAW=zImage-dtb
MODULE=wlan.ko
WLANIMG=wlan.img

if [ "x`uname`" != "xLinux" ] ; then
    ZIMAGE="debian9:/home/sergeym/work/tbone/kernel/arch/arm/boot/$KERNEL_RAW"
    WLAN="debian9:/home/sergeym/work/tbone/kernel/drivers/staging/prima/$MODULE"
else
    ZIMAGE="/home/sergeym/work/tbone/kernel/arch/arm/boot/$KERNEL_RAW"
    WLAN="/home/sergeym/work/tbone/kernel/drivers/staging/prima/$MODULE"
fi
DESTHOST="win10"
DESTDIR="Desktop/tbone"
DEST="$DESTHOST:$DESTDIR"
DEST_LIN=192.168.0.239

MKIMG=../tools/mkbootimg/mkbootimg

echo -n 0\ old\ > md5.sum
md5 $KERNEL >> md5.sum
echo -n 2\ old\ >> md5.sum
md5 $MODULE >> md5.sum
rm $KERNEL
rm $MODULE 

scp $ZIMAGE .
scp $WLAN .

$MKIMG --kernel zImage-dtb --cmdline "`cat cmdline.txt`" --base "81c00000" --pagesize 4096 --kernel_offset "00008000" -o $KERNEL

ls -l $KERNEL
ls -l $MODULE
echo -n 1\ new\ >> md5.sum
md5 $KERNEL >> md5.sum
echo -n 3\ new\ >> md5.sum
md5 $MODULE >> md5.sum
md5 $KERNEL_RAW
sort md5.sum

echo Copying $MODULE to image file...
sudo umount /mnt/tmp >/dev/null 2>&1

if [ ! -e $WLANIMG ] ; then
    dd if=/dev/zero of=$WLANIMG bs=1M count=20 
    mkfs.ext4 -m 0.01 wlan.img
fi

if [ "x`uname`" = "xFreeBSD" ] ; then
    sudo lklfuse -o type=ext4 $WLANIMG /mnt/tmp
    BS=1m
else
    if [ "x`uname`" = "xLinux" ] ; then
	sudo mount $WLANIMG /mnt/tmp
	BS=1M
    else
    	echo Don\'t know how to mount $WLANIMG
    fi
fi
mount | grep -q '/mnt/tmp'
if [ "$?" -eq 0 ] ; then
    xz -kf $MODULE
    sudo cp $MODULE.xz /mnt/tmp/
    sudo umount /mnt/tmp
    dd if=wlan.img of=$KERNEL seek=10 bs=$BS conv=notrunc

    if [ "x$1" = "xl" ] || [ "x$1" = "xL" ] || [ "x$1" = "xw" ] || [ "x$1" = "xW" ] ; then
    	answer=$1
    else
	echo "Copy to Windows/Linux? W | L"
	read answer 
    fi
    if [ "$answer" = "w" ] || [ "$answer" = "W" ] ; then
    	ssh $DESTHOST "attrib -r $DESTDIR/$KERNEL"
	scp $KERNEL $DEST
    else
	if [ "$answer" = "l" ] || [ "$answer" = "L" ] ; then
	    #scp kernel.img 10.8.0.9:/tmp/
	    # temp comm out
	    scp kernel.img exodus@${DEST_LIN}:~/
	    ssh exodus@${DEST_LIN} "sudo dd if=kernel.img of=/dev/disk/by-partlabel/boot ; sync ; sudo reboot"
	    echo Rebooting device...

	fi
    fi
else
    echo Cannot copy $MODULE.xz
fi

